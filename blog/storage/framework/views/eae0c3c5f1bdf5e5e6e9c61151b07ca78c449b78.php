<?php $__env->startSection('title'); ?>
    <h3>Edição de Taxa de Produto</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('action'); ?>
    <?php echo e(url("taxa/update/{$taxa->id}")); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.taxa._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>