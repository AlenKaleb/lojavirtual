<?php $__env->startSection('content'); ?>
    <!-- Select2 -->
    <link href="<?php echo e(asset('panel/assets/vendors/select2/dist/css/select2.min.css')); ?>" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title"><?php echo $__env->yieldContent('title'); ?></h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <form name="formTaxa" action="<?php echo $__env->yieldContent('action'); ?>" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" value="<?php echo e((!empty($taxa->nome)?$taxa->nome:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Valor</label>
                                        <input id="valor" name="valor" type="text" class="form-control" value="<?php echo e((!empty($taxa->valor)?$taxa->valor:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php echo e(csrf_field()); ?>

                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>