<?php $__env->startSection('title'); ?>
    <h3>Cadastro de Cliente Jurídico</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('action'); ?>
    <?php echo e(url("juridico/store")); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.juridico._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>