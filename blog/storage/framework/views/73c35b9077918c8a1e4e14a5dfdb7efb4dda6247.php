<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações Pessoais</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        <div name="formFisico">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="<?php echo e((!empty($clienteFisico->nome)?$clienteFisico->nome:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">CPF</label>
                                        <input id="cpf" name="cpf" type="text" class="form-control" disabled value="<?php echo e((!empty($clienteFisico->cpf)?$clienteFisico->cpf:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="radio">
                                        <label>
                                            <input id="masculino" type="radio" name="sexo" disabled value="M" <?php echo e((!empty($clienteFisico->sexo) && $clienteFisico->sexo == 'M'?'checked="true"':'checked="true"')); ?>>
                                            Masculino
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="feminino" type="radio" name="sexo" disabled value="F" <?php echo e((!empty($clienteFisico->sexo) && $clienteFisico->sexo == 'F'?'checked="true"':null)); ?>>
                                            Feminino
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">RG</label>
                                        <input id="rg" name="rg" type="text" class="form-control" disabled value="<?php echo e((!empty($clienteFisico->rg)?$clienteFisico->rg:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Data de Nascimento</label>
                                        <input id="data_nascimento" name="data_nascimento" type="text" class="form-control datepicker" disabled value="<?php echo e((!empty($clienteFisico->data_nascimento)?$clienteFisico->data_nascimento:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php if(!empty($telefones[0])): ?>
                                    <?php $__currentLoopData = $telefones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $telefone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Telefone</label>
                                                    <input id="telefone" type="text" class="form-control" disabled value="<?php echo e($telefone->telefone); ?>">
                                                </div>
                                            </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cep</label>
                                        <input id="cep" name="cep" type="text" class="form-control" disabled value="<?php echo e($endereco->cep); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pais</label>
                                        <select id="pais" name="pais_id" class="form-control select2-pais" disabled>
                                           <?php if(!empty($endereco->pais_id)): ?>
                                                <option selected value="<?php echo e($endereco->pais_id); ?>"><?php echo e($endereco->nomePais); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Estado</label>
                                        <select id="estado" name="estado_id" class="form-control select2-estado" disabled>
                                            <?php if(!empty($endereco->estado_id)): ?>
                                                <option selected value="<?php echo e($endereco->estado_id); ?>"><?php echo e($endereco->nomeEstado); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cidade</label>
                                        <select id="cidade" name="cidade_id" class="form-control select2-cidade" disabled>
                                            <?php if(!empty($endereco->cidade_id)): ?>
                                                <option selected value="<?php echo e($endereco->cidade_id); ?>"><?php echo e($endereco->nomeCidade); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Logradouro</label>
                                        <input id="logradouro" name="logradouro" type="text" class="form-control" disabled value="<?php echo e($endereco->logradouro); ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Número</label>
                                        <input id="numero" name="numero" type="text" class="form-control" disabled value="<?php echo e($endereco->numero); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Bairro</label>
                                        <input id="bairro" name="bairro" type="text" class="form-control" disabled value="<?php echo e($endereco->bairro); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Complemento</label>
                                        <input id="complemento" name="complemento" type="text" class="form-control" disabled value="<?php echo e($endereco->complemento); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input name="email" type="email" class="form-control" disabled value="<?php echo e((!empty($clienteFisico->email)?$clienteFisico->email:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo e(url("fisico/edit/{$clienteFisico->id}")); ?>" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="<?php echo e(url('fisico/index')); ?>" type="button" class="btn btn-primary pull-right">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>