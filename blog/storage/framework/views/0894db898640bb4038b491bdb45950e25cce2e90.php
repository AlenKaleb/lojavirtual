
<?php $__env->startSection('content'); ?>
    <!-- Select2 -->
    <link href="<?php echo e(asset('panel/assets/vendors/select2/dist/css/select2.min.css')); ?>" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title"><?php echo $__env->yieldContent('title'); ?></h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <form name="formFuncionario" action="<?php echo $__env->yieldContent('action'); ?>" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" value="<?php echo e((!empty($funcionario->nome)?$funcionario->nome:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">CPF</label>
                                        <input id="cpf" name="cpf" type="text" class="form-control" value="<?php echo e((!empty($funcionario->cpf)?$funcionario->cpf:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">RG</label>
                                        <input id="rg" name="rg" type="text" class="form-control" value="<?php echo e((!empty($funcionario->rg)?$funcionario->rg:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="radio">
                                        <label>
                                            <input id="masculino" type="radio" name="sexo" value="M" <?php echo e((!empty($funcionario->sexo) && $funcionario->sexo == 'M'?'checked="true"':'checked="true"')); ?>>
                                            Masculino
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="feminino" type="radio" name="sexo" value="F" <?php echo e((!empty($funcionario->sexo) && $funcionario->sexo == 'F'?'checked="true"':null)); ?>>
                                            Feminino
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input id="email" name="email" type="email" class="form-control" value="<?php echo e((!empty($funcionario->email)?$funcionario->email:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Senha</label>
                                        <input id="password" name="password" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Confirmar Senha</label>
                                        <input id="confirmarPassword" name="confimarPassword" type="password" class="form-control">
                                    </div>
                                </div>
                                <?php echo e(csrf_field()); ?>

                            </div>
                            <input id="user_id" name="user_id" type="hidden" class="form-control" value="<?php echo e((!empty($funcionario->user_id)?$funcionario->user_id:null)); ?>">
                            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>