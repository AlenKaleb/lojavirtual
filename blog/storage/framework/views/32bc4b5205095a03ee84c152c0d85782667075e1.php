<?php $__env->startSection('content'); ?>
<!-- Datatables -->
<link href="<?php echo e(asset('panel/assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>" rel="stylesheet">

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Taxas dos Produtos</h4>
                    <p class="category">Lista de Taxas</p>
                </div>
                <div class="card-content table-responsive">
                    <a href="<?php echo e(url("taxa/create")); ?>">
                        <button class="btn btn-primary btn-fab btn-fab-mini btn-round">
                        <i class="material-icons">add</i>
                    </button> Nova Taxa
                    </a>
                    <table id="datatable-buttons" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Taxa</th>
                            <th>Valor</th>
                            <th>Editar</th>
                            <th>Remover</th>
                        </tr>
                        </thead>


                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="<?php echo e(asset('panel/assets/js/jquery-3.2.1.min.js')); ?>" type="text/javascript"></script>

<!-- Datatables -->
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-buttons/js/buttons.flash.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-buttons/js/buttons.html5.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-buttons/js/buttons.print.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/jszip/dist/jszip.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/pdfmake/build/pdfmake.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/pdfmake/build/vfs_fonts.js')); ?>"></script>

<script type="application/javascript">
    $('#datatable-buttons').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "ordering": true,
        "ajax": {
            "url": "<?php echo e(url('taxa/paginate')); ?>",
            "type": "get",
            "data": function ( d ) {
                return d;
            }
        },
        "columns": [
            { "data": "nome" },
            { "data": "valor" },
            { "data": "editar" },
            { "data": "remover" }
        ]
    } );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>