<?php $__env->startSection('content'); ?>
    <?php
        $subTotal = 0;
        $total = 0;
        $valorTaxas = 0;
    ?>
    <!-- Head Section -->
    <section class="small-section bg-gray-lighter">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-8">
                    <h1 class="hs-line-11 font-alt mb-0">Carrinho</h1>
                </div>

                <div class="col-md-4 mt-30">
                    <div class="mod-breadcrumbs font-alt align-right">
                        <a href="#">Home</a>&nbsp;/&nbsp;<a href="#">Shop</a>&nbsp;/&nbsp;<span>Cart</span>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->


    <!-- Section -->
    <section class="page-section">
        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    <table class="table table-striped shopping-cart-table">
                        <tr>
                            <th class="hidden-xs">
                                Foto
                            </th>
                            <th>
                                Produto
                            </th>
                            <th>
                                Qtd
                            </th>
                            <th>
                                Total
                            </th>
                            <th>

                            </th>
                        </tr>
                        <?php $__currentLoopData = $produtos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $produto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td class="hidden-xs">
                                    <a href="<?php echo e(url("produto/detalhe/{$produto->id}")); ?>"><img src="<?php echo e((!empty($produto->fotos[0])?url("painel/produtofotos/image/{$produto->fotos[0]->nome_gerado}"):null)); ?>" width="60px" alt=""/></a>
                                </td>
                                <td>
                                    <a href="#" title=""><?php echo e($produto->nome); ?></a>
                                </td>
                                <td>
                                    <form class="form">
                                        <input type="number" class="input-sm" style="width: 60px;" min="1" max="1" value="1" />
                                    </form>
                                </td>
                                <td>
                                    R$ <?php echo e(number_format($produto->valor_unitario,2,',','.')); ?>

                                </td>
                                <td>
                                    <a href=""><i class="fa fa-times"></i> <span class="hidden-xs">Remover</span></a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>

                    <hr />

                    <div class="row">
                        <div class="col-sm-8">
                            <form action="#" class="form">
                                <input placeholder="Codigo" class="input-sm mb-xs-10" style="width: 250px;" type="text" pattern=".{3,100}" required />
                                &nbsp;<button type="submit" class="btn btn-mod btn-round btn-gray btn-small mb-xs-10">Aplicar Desconto</button>
                            </form>
                        </div>
                        <div class="col-sm-4 text align-right">

                            <div>
                                <a href="" class="btn btn-mod btn-gray btn-round btn-small">Atualizar Carrinho</a>
                            </div>

                        </div>
                    </div>

                    <hr class="mb-60" />

                    <div class="row">
                        <div class="col-sm-6">

                            <h3 class="small-title font-alt">Taxas</h3>

                            <div class="mb-10">
                                <table class="table table-bordered">
                                    <tr>
                                        <th class="hidden-xs">
                                            Taxa
                                        </th>
                                        <th>
                                            Valor(R$)
                                        </th>
                                    </tr>
                                    <?php $__currentLoopData = $produtos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $produto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $__currentLoopData = $produto->produtoTaxa; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $produtoTaxa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php
                                                $taxa = $produtoTaxa->taxa;
                                                $valorTaxas += $taxa->valor;
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo e($taxa->nome); ?>

                                                </td>
                                                <td>
                                                    <?php echo e(number_format($taxa->valor,2,',','.')); ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $subTotal += $produto->valor_unitario;
                                        ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <strong>Total:</strong>
                                        </td>
                                        <td>
                                            <strong>R$ <?php echo e(number_format($valorTaxas,2,',','.')); ?></strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-6 text align-right pt-10">

                            <?php
                                $total += ($subTotal + $valorTaxas);
                            ?>
                            <div>
                                Subtotal: <strong>R$ <?php echo e(number_format($subTotal,2,',','.')); ?></strong>
                            </div>

                            <div class="mb-10">
                                Taxas: <strong>R$ <?php echo e(number_format($valorTaxas,2,',','.')); ?></strong>
                            </div>

                            <div class="lead mt-0 mb-30">
                                Valor Total: <strong>R$ <?php echo e(number_format($total,2,',','.')); ?></strong>
                            </div>

                            <div>
                                <a href="" class="btn btn-mod btn-round btn-large">Finalizar Compra</a>
                            </div>

                        </div>
                    </div>



                </div>
            </div>

        </div>
    </section>
    <!-- End Section -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('site.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>