<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações da ticket</h4>
                        <p class="category">ticket!</p>
                    </div>
                    <div class="card-content">
                        <div name="formTicket">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="<?php echo e($ticket->nome); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Codigo</label>
                                        <input id="codigo" name="codigo" type="text" class="form-control" disabled value="<?php echo e($ticket->codigo); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Desconto</label>
                                        <input id="desconto" name="desconto" type="text" class="form-control" disabled value="<?php echo e($ticket->desconto); ?>">
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo e(url("ticket/edit/{$ticket->id}")); ?>" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="<?php echo e(url('ticket/index')); ?>" type="button" class="btn btn-primary pull-right">Voltar</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>