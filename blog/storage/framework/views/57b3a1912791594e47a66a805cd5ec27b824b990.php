<?php $__env->startSection('title'); ?>
    <h3>Cadastro de Funcionário</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('action'); ?>
    <?php echo e(url("funcionario/store")); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.funcionario._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>