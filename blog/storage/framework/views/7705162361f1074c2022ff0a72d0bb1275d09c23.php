
<?php $__env->startSection('content'); ?>
    <!-- Head Section -->
    <section class="small-section bg-gray-lighter">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-8">
                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">Produtos</h1>
                    <div class="hs-line-4 font-alt black">
                        Produtos da AWKA Thechnologic
                    </div>
                </div>

                <div class="col-md-4 mt-30">
                    <div class="mod-breadcrumbs font-alt align-right">
                        <a href="#">Inicio</a>&nbsp;/&nbsp;<a href="#">Produtos</a>&nbsp;
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->

    <!-- Section -->
    <section class="page-section">
        <div class="container relative">

            <div class="row">

                <!-- Content -->
                <div class="col-sm-8">

                    <!-- Shop options -->
                    <div class="clearfix mb-40">

                        <div class="left section-text mt-10">
                            Showing 1–12 of 23 results
                        </div>

                        <div class="right">
                            <form method="post" action="#" class="form">
                                <select class="input-md round">
                                    <option>Default sorting</option>
                                    <option>Sort by price: low to high</option>
                                    <option>Sort by price: high to low</option>
                                </select>
                            </form>
                        </div>

                    </div>
                    <!-- End Shop options -->

                    <div class="row multi-columns-row">

                        <?php $__currentLoopData = $produtos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $produto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <!-- Shop Item -->
                            <div class="col-md-6 col-lg-6 mb-60 mb-xs-40">

                                <div class="post-prev-img">

                                    <?php if(!empty($produto->fotos[0])): ?>
                                        <a href="<?php echo e(url("produto/detalhe/{$produto->id}")); ?>"><img src="<?php echo e(url("painel/produtofotos/image/{$produto->fotos[0]->nome_gerado}")); ?>" style="width: 500px; height: 250px;" alt="" /></a>
                                    <?php endif; ?>

                                    <div class="intro-label">
                                        <span class="label label-danger bg-red">LANÇAMENTO</span>
                                    </div>

                                </div>

                                <div class="post-prev-title font-alt align-center">
                                    <a href="<?php echo e(url("produto/detalhe/{$produto->id}")); ?>"><?php echo e($produto->nome); ?></a>
                                </div>

                                <div class="post-prev-text align-center">
                                    <del>$150.00</del>
                                    &nbsp;
                                    <strong><?php echo e($produto->valor_unitario); ?></strong>
                                </div>

                                <div class="post-prev-more align-center">
                                    <form id="formProduto-<?php echo e($produto->id); ?>" name="formProduto">
                                        <input id="produto-<?php echo e($produto->id); ?>" name="produto_id" type="hidden" value="<?php echo e($produto->id); ?>">
                                        <?php echo e(csrf_field()); ?>

                                        <?php $add = 0; ?>
                                        <?php if(!empty($carrinhoProdutos[0])): ?>
                                            <?php $__currentLoopData = $carrinhoProdutos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carrinhoProduto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($produto->id == $carrinhoProduto->produto_id): ?>
                                                    <?php
                                                        $add = 1;
                                                    ?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                        <button id="buttonProduto-<?php echo e($produto->id); ?>" type="button" class="btn btn-mod <?php echo e(($add == 0?'btn-gray':null)); ?> btn-round add-carrinho" <?php echo e(($add == 1?'disabled':null)); ?>><i class="fa fa-shopping-cart"></i> <?php echo e(($add == 0?'Adicionar ao carrinho':'Produto adicionado ao carrinho')); ?></button>
                                    </form>
                                </div>

                            </div>
                            <!-- End Shop Item -->

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                    <!-- Pagination -->
                    <div class="pagination">
                        <a href=""><i class="fa fa-angle-left"></i></a>
                        <a href="" class="active">1</a>
                        <a href="">2</a>
                        <a href="">3</a>
                        <a class="no-active">...</a>
                        <a href="">9</a>
                        <a href=""><i class="fa fa-angle-right"></i></a>
                    </div>
                    <!-- End Pagination -->

                </div>
                <!-- End Content -->

                <!-- Sidebar -->
                <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">

                    <!-- Widget -->
                    <div class="widget">

                        <h5 class="widget-title font-alt">Carrinho</h5>

                        <div class="widget-body">
                            <ul id="divCarrinho" class="clearlist widget-posts">

                                <!-- Preview item -->
                                <li id="divProdutoCarrinho" class="clearfix">
                                    <a id="linkProdutoCarrinho" href=""><img id="imagemProdutoCarrinho" width="70px" src="" alt="" class="widget-posts-img" /></a>
                                    <div class="widget-posts-descr">
                                        <a id="linkNomeProdutoCarrinho" href="#" title=""></a>
                                        <div id="divValorProdutoCarrinho">

                                        </div>
                                        <div>
                                            <a id="linkRemoverProdutoCarrinho" class="removerProdutoCarrinho" href="#remover"><i class="fa fa-times"></i> Remover</a>
                                        </div>
                                    </div>
                                </li>
                                <!-- End Preview item -->

                            </ul>

                            <div class="clearfix mt-20">

                                <div class="left mt-10">
                                    Subtotal: <strong id="subTotal">$35.00</strong>
                                </div>

                                <div class="right">
                                    <a href="<?php echo e(url('carrinho/index')); ?>" class="btn btn-mod btn-border btn-small btn-round">Ver Carrinho</a>
                                </div>

                            </div>

                            <div>

                            </div>

                        </div>

                    </div>
                    <!-- End Widget -->

                    <!-- Widget -->
                    <div class="widget">

                        <h5 class="widget-title font-alt">Filtro por preço</h5>

                        <div class="widget-body">
                            <form method="post" action="#" class="form">

                                <div class="row mb-20">
                                    <div class="col-xs-6">
                                        <input type="text" name="price-from" id="price-from" class="input-md round form-control" placeholder="From, $" maxlength="100">
                                    </div>

                                    <div class="col-xs-6">
                                        <input type="text" name="price-to" id="price-to" class="input-md round form-control" placeholder="To, $" maxlength="100">
                                    </div>
                                </div>

                                <button class="btn btn-mod btn-medium btn-full btn-round">Filtrar</button>

                            </form>
                        </div>

                    </div>
                    <!-- End Widget -->

                    <!-- Widget -->
                    <div class="widget">

                        <h5 class="widget-title font-alt">Categorias</h5>

                        <div class="widget-body">
                            <ul class="clearlist widget-menu">
                                <?php $__currentLoopData = $categorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="#" title=""><?php echo e($categoria->nome); ?></a>
                                        <small>
                                            - <?php echo e($categoria->qtdProduto); ?>

                                        </small>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>

                    </div>
                    <!-- End Widget -->

                    <!-- Widget -->
                    <div class="widget">

                        <h5 class="widget-title font-alt">Tags</h5>

                        <div class="widget-body">
                            <div class="tags">
                                <?php if(!empty($carrinhoProdutos[0])): ?>
                                    <?php $__currentLoopData = $carrinhoProdutos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carrinhoProduto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $tagTecnologias = explode(',',$carrinhoProduto->tag_tecnologia);
                                        ?>
                                            <?php $__currentLoopData = $tagTecnologias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tagTecnologia): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <a href=""><?php echo e($tagTecnologia); ?></a>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                    <!-- End Widget -->

                </div>
                <!-- End Sidebar -->
            </div>

        </div>
    </section>
    <!-- End Section -->

    <!--   Core JS Files   -->
    <script src="<?php echo e(asset('panel/assets/js/jquery-3.2.1.min.js')); ?>" type="text/javascript"></script>

    <script type="application/javascript">
    jQuery(function ($) {
            $(".add-carrinho").click(function(){
        //        event.preventDefault();
        //                           alert('ok');
                var idArr = this.id.split('-');
    //            alert(idArr[1]);
                $.ajax({
                    url:"<?php echo e(url('carrinho/store')); ?>",
                    method: 'POST',
                    data: $("#formProduto-"+idArr[1]).serialize(),
                    success: function( data ) {
    //                    alert(data.id);
                        if(data != ''){
                            adicionarProdutoCarrinho(data.carrinho_id,data.produto_id,data.nome_gerado,data.nome,data.valor_unitario);
                            $('#buttonProduto-'+data.produto_id).removeAttr('class').attr('class','btn btn-mod btn-round add-carrinho').html('<i class="fa fa-shopping-cart"></i> Produto adicionado ao carrinho').prop('disabled',true);
                        }
                    },
                    error: function(xhr, status, error) {

                    }
                });
            });

            $("#divProdutoCarrinho").prop('disabled',true).hide();
            var count = 0;
            <?php if(!empty($produto->id)): ?>
                <?php $__currentLoopData = $carrinhoProdutos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $carrinhoProduto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    adicionarProdutoCarrinho("<?php echo e($carrinhoProduto->carrinho_id); ?>","<?php echo e($carrinhoProduto->produto_id); ?>","<?php echo e((!empty($carrinhoProduto->fotos[0])?$carrinhoProduto->fotos[0]->nome_gerado:null)); ?>","<?php echo e($carrinhoProduto->nome); ?>","<?php echo e($carrinhoProduto->valor_unitario); ?>");
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            function adicionarProdutoCarrinho(carrinho_id,produto_id,produto_foto_nome,produto_nome,produto_valor_unitario){
                var div = $("#divProdutoCarrinho").clone().prop('disabed',false).show();
                div.removeAttr('id').attr('id', "divProdutoCarrinho" + count);
                div.find("a[id='linkProdutoCarrinho']").removeAttr('id').removeAttr('href').attr('id', 'linkProdutoCarrinho' + count).attr('href', "<?php echo e(url('carrinho/index')); ?>");
                div.find("img[id='imagemProdutoCarrinho']").removeAttr('id').removeAttr('href').attr('id', 'imagemProdutoCarrinho' + count).attr('src', "<?php echo e(url("painel/produtofotos/image")); ?>/"+produto_foto_nome);
                div.find("a[id='linkNomeProdutoCarrinho']").removeAttr('id').removeAttr('href').attr('id', 'linkNomeProdutoCarrinho' + count).attr('href', "<?php echo e(url('carrinho/index')); ?>").text(produto_nome);
                div.find("div[id='divValorProdutoCarrinho']").removeAttr('id').attr('id', 'divValorProdutoCarrinho' + count).text(produto_valor_unitario);

                div.find("a[id='linkRemoverProdutoCarrinho']").removeAttr('id').attr('id', 'linkRemoverProdutoCarrinho-' + count);
                div.appendTo('#divCarrinho');

                $("#linkRemoverProdutoCarrinho-"+count).click(function () {
                    var posicao = this.id.split('-');
                    $.ajax({
                        url:"<?php echo e(url("carrinho/delete/")); ?>/"+carrinho_id+'/'+produto_id,
                        method: 'GET',
                        data: [],
                        success: function( data ) {
                            //                    alert(data.id);
                            removerProdutoCarrinho(posicao[1]);
                            $('#buttonProduto-'+produto_id).removeAttr('class').attr('class','btn btn-mod btn-gray btn-round add-carrinho').html('<i class="fa fa-shopping-cart"></i> Adicionar ao carrinho').prop('disabled',false);
                        },
                        error: function(xhr, status, error) {

                        }
                    });
                });

                count += 1;
            }

            function removerProdutoCarrinho(count){
                $("#divProdutoCarrinho"+count).remove();
            }

//            $(".add-carrinho").click(function () {
//
//            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('site.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>