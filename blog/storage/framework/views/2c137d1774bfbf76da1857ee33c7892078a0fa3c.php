<?php $__env->startSection('title'); ?>
    <h3>Edição de Forma de Pagamento</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('action'); ?>
    <?php echo e(url("formapagamento/update/{$formaPagamento->id}")); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.formapagamento._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>