<!DOCTYPE html>
<html>
<head>
    <title>Loja Virtual</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="Roman Kirichik">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- Favicons -->
    <link rel="shortcut icon" href="<?php echo e(url('site/images/favicon.png')); ?>">
    <link rel="apple-touch-icon" href="<?php echo e(url('site/images/apple-touch-icon.png')); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(url('site/images/apple-touch-icon-72x72.png')); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(url('site/images/apple-touch-icon-114x114.png')); ?>">

    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo e(url('site/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('site/css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('site/css/style-responsive.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('site/css/animate.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('site/css/vertical-rhythm.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('site/css/owl.carousel.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('site/css/magnific-popup.css')); ?>">


</head>
<body class="appear-animate">

<?php
    $categorias = \App\Categoria::select('id','nome')->get();
?>

<!-- Page Loader -->
<div class="page-loader">
    <div class="loader">Carregando...</div>
</div>
<!-- End Page Loader -->

<!-- Page Wrap -->
<div class="page" id="top">

    <!-- Navigation panel -->
    <nav class="main-nav js-stick">
        <div class="full-wrapper relative clearfix">
            <!-- Logo ( * your text or image into link tag *) -->
            <div class="nav-logo-wrap local-scroll">
                <a href="mp-index.html" class="logo">
                    <img src="<?php echo e(url('site/images/logo-dark.png')); ?>" alt="" />
                </a>
            </div>
            <div class="mobile-nav">
                <i class="fa fa-bars"></i>
            </div>

            <!-- Main Menu -->
            <div class="inner-nav desktop-nav">
                <ul class="clearlist">

                    <!-- Item With Sub -->
                    <li>
                        <a href="#" class="mn-has-sub">Home</a>

                    </li>
                    <!-- End Item With Sub -->

                    <!-- Item With Sub -->
                    <li>
                        <a href="#" class="mn-has-sub">Empresa <i class="fa fa-angle-down"></i></a>

                        <!-- Sub Multilevel -->
                        <ul class="mn-sub mn-has-multi">

                            <li class="mn-sub-multi">

                                <ul>
                                    <li>
                                        <a href="elements-accordions.html">Quem Somos</a>
                                    </li>
                                    <li>
                                        <a href="elements-bars.html">Serviços</a>
                                    </li>
                                    <li>
                                        <a href="elements-buttons.html">Trabalhos</a>
                                    </li>
                                </ul>

                            </li>

                        </ul>
                        <!-- End Sub Multilevel -->

                    </li>
                    <!-- End Item With Sub -->

                    <!-- Item With Sub -->
                    <li>
                        <a href="#" class="mn-has-sub">Categoria <i class="fa fa-angle-down"></i></a>

                        <!-- Sub Multilevel -->
                        <ul class="mn-sub mn-has-multi">

                            <li class="mn-sub-multi">
                                <a class="mn-group-title">Categorias</a>

                                <ul>
                                    <?php $__currentLoopData = $categorias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $categoria): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href="#"><?php echo e($categoria->nome); ?></a>
                                        </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>

                            </li>

                        </ul>
                        <!-- End Sub Multilevel -->

                    </li>
                    <!-- End Item With Sub -->

                    <!-- Item With Sub -->
                    <li>
                        <a href="#" class="mn-has-sub active">Loja <i class="fa fa-angle-down"></i></a>

                        <!-- Sub -->
                        <ul class="mn-sub to-left">

                            <li>
                                <a href="<?php echo e(url('produtos/index')); ?>">Produtos</a>
                            </li>
                            <li>
                                <a href="<?php echo e(url('carrinho/index')); ?>">Carrinho</a>
                            </li>

                        </ul>
                        <!-- End Sub -->

                    </li>
                    <!-- End Item With Sub -->

                    <!-- Divider -->
                    <li><a>&nbsp;</a></li>
                    <!-- End Divider -->

                    <!-- Search -->
                    <li>
                        <a href="#" class="mn-has-sub"><i class="fa fa-search"></i> Pesquisar</a>

                        <ul class="mn-sub">

                            <li>
                                <div class="mn-wrap">
                                    <form method="post" class="form">
                                        <div class="search-wrap">
                                            <button class="search-button animate" type="submit" title="Start Search">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <input type="text" class="form-control search-field" placeholder="Search...">
                                        </div>
                                    </form>
                                </div>
                            </li>

                        </ul>

                    </li>
                    <!-- End Search -->

                    <!-- Cart -->
                    <li>
                        <a href="#" class="active"><i class="fa fa-shopping-cart"></i> Carrinho(1)</a>
                    </li>
                    <!-- End Cart -->

                </ul>
            </div>
            <!-- End Main Menu -->


        </div>
    </nav>
    <!-- End Navigation panel -->


    <?php echo $__env->yieldContent('content'); ?>


            <!-- Foter -->
    <footer class="page-section bg-gray-lighter footer pb-60">
        <div class="container">

            <!-- Footer Logo -->
            <div class="local-scroll mb-30 wow fadeInUp" data-wow-duration="1.5s">
                <a href="#top"><img src="<?php echo e(url('site/images/logo-footer.png')); ?>" width="78" height="36" alt="" /></a>
            </div>
            <!-- End Footer Logo -->

            <!-- Social Links -->
            <div class="footer-social-links mb-110 mb-xs-60">
                <a href="#" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                <a href="#" title="Behance" target="_blank"><i class="fa fa-behance"></i></a>
                <a href="#" title="LinkedIn+" target="_blank"><i class="fa fa-linkedin"></i></a>
                <a href="#" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>
            </div>
            <!-- End Social Links -->

            <!-- Footer Text -->
            <div class="footer-text">

                <!-- Copyright -->
                <div class="footer-copy font-alt">
                    <a href="http://themeforest.net/user/theme-guru/portfolio" target="_blank">&copy; Rhythm 2016</a>.
                </div>
                <!-- End Copyright -->

                <div class="footer-made">
                    Made with love for great people.
                </div>

            </div>
            <!-- End Footer Text -->

        </div>


        <!-- Top Link -->
        <div class="local-scroll">
            <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
        </div>
        <!-- End Top Link -->

    </footer>
    <!-- End Foter -->


</div>
<!-- End Page Wrap -->


<!-- JS -->
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery-1.11.2.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.easing.1.3.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/SmoothScroll.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.scrollTo.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.localScroll.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.viewport.mini.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.countTo.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.appear.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.sticky.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.parallax-1.1.3.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.fitvids.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/owl.carousel.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/isotope.pkgd.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/imagesloaded.pkgd.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.magnific-popup.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/gmap3.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/wow.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/masonry.pkgd.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.simple-text-rotator.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/all.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/contact-form.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('site/js/jquery.ajaxchimp.min.js')); ?>"></script>
<!--[if lt IE 10]><script type="text/javascript" src="<?php echo e(asset('site/js/placeholder.js')); ?>"></script><![endif]-->

</body>
</html>
