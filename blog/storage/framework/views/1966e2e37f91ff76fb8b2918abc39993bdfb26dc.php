<?php $__env->startSection('content'); ?>

    <!-- Bootstrap TagsInput -->
    <link href="<?php echo e(asset('panel/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')); ?>" rel="stylesheet">

<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações do produto</h4>
                        <p class="category">produto!</p>
                    </div>
                    <div class="card-content">
                        <div name="formTaxa">
                            <div class="row">
                                <?php $__currentLoopData = $fotos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $foto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-sm-2">
                                        <h4>Foto <?php echo e($key); ?></h4>
                                        <img src="<?php echo e(url("painel/produtofotos/image/{$foto->nome_gerado}")); ?>" alt="Foto <?php echo e($key); ?>" class="rounded img-fluid">
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="togglebutton">
                                        <label>
                                            <input name="status" type="checkbox" <?php echo e(($produto->status == 1?'checked=""':null)); ?> disabled value="1">
                                            Status
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="<?php echo e((!empty($produto->nome)?$produto->nome:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Categoria</label>
                                        <select id="categoria_id" name="categoria_id" class="form-control select2-categoria" disabled>
                                            <option selected>Selecione uma categoria</option>
                                            <?php if(!empty($produto->categoria_id)): ?>
                                                <option selected><?php echo e($produto->nomeCategoria); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Qtd</label>
                                        <input id="qtd" name="qtd" type="text" class="form-control" disabled value="<?php echo e((!empty($produto->qtd)?$produto->qtd:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Valor Unitário</label>
                                        <input id="valor_unitario" name="valor_unitario" type="text" class="form-control" disabled value="<?php echo e((!empty($produto->valor_unitario)?$produto->valor_unitario:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php $__currentLoopData = $taxas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $taxa): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="col-md-5">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Taxa</label>
                                            <select id="taxa" name="taxa_id[]" class="form-control select2-taxa" disabled>
                                                <?php if(!empty($taxa->id)): ?>
                                                    <option selected><?php echo e($taxa->nome); ?></option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating btn-group" data-toggle="buttons">
                                        <label class="control-label">Compexidade</label>
                                        <label class="btn btn-default btn-sm <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 1?'active':(empty($produto->complexidade)?'active':null))); ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="1" <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 1?'checked':(empty($produto->complexidade)?'checked':null))); ?>> &nbsp; 1 &nbsp;
                                        </label>
                                        <label class="btn btn-success btn-sm <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 2?'active':null)); ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="2" <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 2?'checked':null)); ?>> &nbsp; 2 &nbsp;
                                        </label>
                                        <label class="btn btn-primary btn-sm <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 3?'active':null)); ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="3" <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 3?'checked':null)); ?>> &nbsp; 3 &nbsp;
                                        </label>
                                        <label class="btn btn-warning btn-sm <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 4?'active':null)); ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="4" <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 4?'checked':null)); ?>> &nbsp; 4 &nbsp;
                                        </label>
                                        <label class="btn btn-danger btn-sm <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 5?'active':null)); ?>" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="5" <?php echo e((!empty($produto->complexidade) && $produto->complexidade == 5?'checked':null)); ?>> &nbsp; 5 &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label">Tags de Tecnologias</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_tecnologia" name="tag_tecnologia" type="text" class="form-control" data-role="tagsinput" disabled value="<?php echo e((!empty($produto->tag_tecnologia)?$produto->tag_tecnologia:null)); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Tags de Vantagens</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_vantagem" name="tag_vantagem" type="text" class="form-control" data-role="tagsinput" disabled value="<?php echo e((!empty($produto->tag_vantagem)?$produto->tag_vantagem:null)); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Tags de Desvantagens</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_desvantagem" name="tag_desvantagem" type="text" class="form-control" data-role="tagsinput" disabled value="<?php echo e((!empty($produto->tag_desvantagem)?$produto->tag_desvantagem:null)); ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Descrição</label>
                                        <div class="form-group label-floating">
                                            <label class="control-label"> Insira detalhes sobre o produto</label>
                                            <textarea name="descricao" class="form-control" rows="5" disabled><?php echo e((!empty($produto->descricao)?$produto->descricao:null)); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo e(url("produto/edit/{$produto->id}")); ?>" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="<?php echo e(url('produto/index')); ?>" type="button" class="btn btn-primary pull-right">Voltar</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="<?php echo e(asset('panel/assets/js/jquery-3.2.1.min.js')); ?>" type="text/javascript"></script>

    <!-- Bootstrap TagsInput -->
    <script src="<?php echo e(asset('panel/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.js')); ?>"></script>
<script type="application/javascript">
    $(".bootstrap-tagsinput > input").prop("disabled", true);
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>