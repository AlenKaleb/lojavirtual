<?php $__env->startSection('content'); ?>
    <!-- Select2 -->
    <link href="<?php echo e(asset('panel/assets/vendors/select2/dist/css/select2.min.css')); ?>" rel="stylesheet">

    <!-- DateTimePicker -->
    <link href="<?php echo e(asset('panel/assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')); ?>" rel="stylesheet">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title"><?php echo $__env->yieldContent('title'); ?></h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        <?php if($errors->any()): ?>
                            <div class="alert alert-danger">
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                        <form name="formJuridico" action="<?php echo $__env->yieldContent('action'); ?>" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Razão Social</label>
                                        <input id="razao_social" name="razao_social" type="text" class="form-control" value="<?php echo e((!empty($clienteJuridico->razao_social)?$clienteJuridico->razao_social:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome Fantasia</label>
                                        <input id="nome_fantasia" name="nome_fantasia" type="text" class="form-control" value="<?php echo e((!empty($clienteJuridico->nome_fantasia)?$clienteJuridico->nome_fantasia:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">CNPJ</label>
                                        <input id="cnpj" name="cnpj" type="text" class="form-control" value="<?php echo e((!empty($clienteJuridico->cnpj)?$clienteJuridico->cnpj:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Data de Registro</label>
                                        <input id="data_registro" name="data_registro" type="text" class="form-control datepicker" value="<?php echo e((!empty($clienteJuridico->data_registro)?$clienteJuridico->data_registro:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Inscrição Estadual</label>
                                        <input id="inscricao_estadual" name="inscricao_estadual" type="text" class="form-control" value="<?php echo e((!empty($clienteJuridico->inscricao_estadual)?$clienteJuridico->inscricao_estadual:null)); ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="divTelefones">
                                    <div id="divTelefone">
                                        <div class="col-md-5">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Telefone</label>
                                                <input id="telefone" name="telefone[]" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button id="removerTelefone" type="button" class="btn btn-primary btn-just-icon removerTelefone">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button id="novoTelefone" type="button" class="btn btn-success">Adicionar Telefone</button>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cep</label>
                                        <input id="cep" name="cep" type="text" class="form-control" value="<?php echo e((!empty($endereco->cep)?$endereco->cep:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pais</label>
                                        <select id="pais" name="pais_id" class="form-control select2-pais">
                                            <option selected>Selecione um país</option>
                                            <?php if(!empty($paises[0])): ?>
                                                <?php $__currentLoopData = $paises; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pais): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if(!empty($endereco->pais_id) && $pais->id == $endereco->pais_id): ?>
                                                        <option selected value="<?php echo e($pais->id); ?>"><?php echo e($pais->nome_pt); ?></option>
                                                    <?php else: ?>
                                                        <option value="<?php echo e($pais->id); ?>"><?php echo e($pais->nome_pt); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Estado</label>
                                        <select id="estado" name="estado_id" class="form-control select2-estado">
                                            <option selected>Selecione um estado</option>
                                            <?php if(!empty($endereco->estado_id)): ?>
                                                <option selected value="<?php echo e($endereco->estado_id); ?>"><?php echo e($endereco->nomeEstado); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cidade</label>
                                        <select id="cidade" name="cidade_id" class="form-control select2-cidade">
                                            <option selected>Selecione uma cidade</option>
                                            <?php if(!empty($endereco->cidade_id)): ?>
                                                <option selected value="<?php echo e($endereco->cidade_id); ?>"><?php echo e($endereco->nomeCidade); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Logradouro</label>
                                        <input id="logradouro" name="logradouro" type="text" class="form-control" value="<?php echo e((!empty($endereco->logradouro)?$endereco->logradouro:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Número</label>
                                        <input id="numero" name="numero" type="text" class="form-control" value="<?php echo e((!empty($endereco->numero)?$endereco->numero:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Bairro</label>
                                        <input id="bairro" name="bairro" type="text" class="form-control" value="<?php echo e((!empty($endereco->bairro)?$endereco->bairro:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Complemento</label>
                                        <input id="complemento" name="complemento" type="text" class="form-control" value="<?php echo e((!empty($endereco->complemento)?$endereco->complemento:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input name="email" type="email" class="form-control" value="<?php echo e((!empty($clienteJuridico->email)?$clienteJuridico->email:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Senha</label>
                                        <input id="password" name="password" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Confirmar Senha</label>
                                        <input id="confirmarPassword" name="confimarPassword" type="password" class="form-control">
                                    </div>
                                </div>
                                <?php echo e(csrf_field()); ?>

                            </div>
                            <input id="user_id" name="user_id" type="hidden" class="form-control" value="<?php echo e((!empty($clienteJuridico->user_id)?$clienteJuridico->user_id:null)); ?>">
                            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--   Core JS Files   -->
<script src="<?php echo e(asset('panel/assets/js/jquery-3.2.1.min.js')); ?>" type="text/javascript"></script>

<!-- Select2 -->
<script src="<?php echo e(asset('panel/assets/vendors/select2/dist/js/select2.min.js')); ?>"></script>

<!-- bootstrap-daterangepicker -->
<script src="<?php echo e(asset('panel/assets/vendors/moment/min/moment.min.js')); ?>"></script>
<script src="<?php echo e(asset('panel/assets/vendors/moment/locale/pt-br.js')); ?>"></script>


<!-- DateTimePicker -->
<script src="<?php echo e(asset('panel/assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')); ?>"></script>

<script type="application/javascript">

    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("#cep").change(function () {
        var cep_code = $(this).val();
        if (cep_code.length <= 0)
            return;
        $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", {code: cep_code},
                function (result) {
                    if (result.status != 1) {
                        alert(result.message || "Houve um erro desconhecido");
                        return;
                    }
                    $("input#cep").val(result.code);
                    $("input#bairro").val(result.district);
                    $("input#logradouro").val(result.address);
                });
    });
    $(".select2-pais").select2({
        tags: false,
        multiple: false,
        minimumInputLength: 2,
        ajax: {
            url: "<?php echo e(url('pais/pesquisarPais')); ?>",
            dataType: "json",
            type: "GET",
            data: function (params) {

                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $(".select2-estado").select2({
        tags: false,
        multiple: false,
        minimumInputLength: 2,
        ajax: {
            url: "<?php echo e(url('estado/pesquisarEstado')); ?>",
            dataType: "json",
            type: "GET",
            data: function (params) {

                var queryParameters = {
                    term: params.term,
                    pais_id: $("#pais").val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $(".select2-cidade").select2({
        tags: false,
        multiple: false,
        minimumInputLength: 2,
        ajax: {
            url: "<?php echo e(url('cidade/pesquisarCidade')); ?>",
            dataType: "json",
            type: "GET",
            data: function (params) {

                var queryParameters = {
                    term: params.term,
                    estado_id: $("#estado").val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    jQuery(function ($) {
        $("#divTelefone").prop('disabled',true).hide();
        var count = 0;
        <?php if(!empty($clienteJuridico->id)): ?>
            <?php $__currentLoopData = $telefones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $telefone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    adicionarTelefone("<?php echo e($telefone->telefone); ?>");
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
    function adicionarTelefone(telefone){
            var div = $("#divTelefone").clone().prop('disabed',false).show();
            div.removeAttr('id').attr('id', "divTelefone" + count);
            div.find("input[id='telefone']").removeAttr('id').removeAttr('name').attr('id', 'telefone' + count).attr('name', "telefone[" + count + "]").val(telefone);
            div.find("button[id='removerTelefone']").removeAttr('id').attr('id', 'removerTelefone-' + count);
            div.appendTo('#divTelefones');

            count += 1;

            $(".removerTelefone").click(function () {
                var posicao = this.id.split('-');
                removerTelefone(posicao[1]);
            });
        }

        function removerTelefone(count){
            $("#divTelefone"+count).remove();
        }

        $("#novoTelefone").click(function () {
            adicionarTelefone('');
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>