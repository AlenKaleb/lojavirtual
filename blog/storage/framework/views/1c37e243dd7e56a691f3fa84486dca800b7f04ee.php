<?php $__env->startSection('title'); ?>
    <h3>Cadastro de Categoria de Produto</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('action'); ?>
    <?php echo e(url("categoria/store")); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.categoria._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>