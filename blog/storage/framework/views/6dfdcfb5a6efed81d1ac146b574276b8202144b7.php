<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações Pessoais</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        <div name="formJuridico">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Razão Social</label>
                                        <input id="razao_social" name="razao_social" type="text" class="form-control" disabled value="<?php echo e((!empty($clienteJuridico->razao_social)?$clienteJuridico->razao_social:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome Fantasia</label>
                                        <input id="nome_fantasia" name="nome_fantasia" type="text" class="form-control" disabled value="<?php echo e((!empty($clienteJuridico->nome_fantasia)?$clienteJuridico->nome_fantasia:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">CNPJ</label>
                                        <input id="cnpj" name="cnpj" type="text" class="form-control" disabled value="<?php echo e((!empty($clienteJuridico->cnpj)?$clienteJuridico->cnpj:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Data de Registro</label>
                                        <input id="data_registro" name="data_registro" type="text" class="form-control datepicker" disabled value="<?php echo e((!empty($clienteJuridico->data_registro)?$clienteJuridico->data_registro:null)); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Inscrição Estadual</label>
                                        <input id="inscricao_estadual" name="inscricao_estadual" type="text" class="form-control" disabled value="<?php echo e((!empty($clienteJuridico->inscricao_estadual)?$clienteJuridico->inscricao_estadual:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php if(!empty($telefones[0])): ?>
                                    <?php $__currentLoopData = $telefones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $telefone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Telefone</label>
                                                    <input id="telefone" type="text" class="form-control" disabled value="<?php echo e($telefone->telefone); ?>">
                                                </div>
                                            </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cep</label>
                                        <input id="cep" name="cep" type="text" class="form-control" disabled value="<?php echo e($endereco->cep); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pais</label>
                                        <select id="pais" name="pais_id" class="form-control select2-pais" disabled>
                                           <?php if(!empty($endereco->pais_id)): ?>
                                                <option selected value="<?php echo e($endereco->pais_id); ?>"><?php echo e($endereco->nomePais); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Estado</label>
                                        <select id="estado" name="estado_id" class="form-control select2-estado" disabled>
                                            <?php if(!empty($endereco->estado_id)): ?>
                                                <option selected value="<?php echo e($endereco->estado_id); ?>"><?php echo e($endereco->nomeEstado); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cidade</label>
                                        <select id="cidade" name="cidade_id" class="form-control select2-cidade" disabled>
                                            <?php if(!empty($endereco->cidade_id)): ?>
                                                <option selected value="<?php echo e($endereco->cidade_id); ?>"><?php echo e($endereco->nomeCidade); ?></option>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Logradouro</label>
                                        <input id="logradouro" name="logradouro" type="text" class="form-control" disabled value="<?php echo e($endereco->logradouro); ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Número</label>
                                        <input id="numero" name="numero" type="text" class="form-control" disabled value="<?php echo e($endereco->numero); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Bairro</label>
                                        <input id="bairro" name="bairro" type="text" class="form-control" disabled value="<?php echo e($endereco->bairro); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Complemento</label>
                                        <input id="complemento" name="complemento" type="text" class="form-control" disabled value="<?php echo e($endereco->complemento); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input name="email" type="email" class="form-control" disabled value="<?php echo e((!empty($clienteJuridico->email)?$clienteJuridico->email:null)); ?>">
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo e(url("juridico/edit/{$clienteJuridico->id}")); ?>" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="<?php echo e(url('juridico/index')); ?>" type="button" class="btn btn-primary pull-right">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>