
<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações Pessoais</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        <div name="formFuncionario">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="<?php echo e($funcionario->nome); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">CPF</label>
                                        <input id="cpf" name="cpf" type="text" class="form-control" disabled value="<?php echo e($funcionario->cpf); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">RG</label>
                                        <input id="rg" name="rg" type="text" class="form-control" disabled value="<?php echo e($funcionario->rg); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="radio">
                                        <label>
                                            <input id="masculino" type="radio" name="sexo" disabled value="M" <?php echo e(($funcionario->sexo == 'M'?'checked="true"':null)); ?>>
                                            Masculino
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="feminino" type="radio" name="sexo" disabled value="F" <?php echo e(($funcionario->sexo == 'F'?'checked="true"':null)); ?>>
                                            Feminino
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input id="email" name="email" type="email" class="form-control" disabled value="<?php echo e($funcionario->email); ?>">
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo e(url("funcionario/edit/{$funcionario->id}")); ?>" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="<?php echo e(url('funcionario/index')); ?>" type="button" class="btn btn-primary pull-right">Voltar</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>