<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações da taxa</h4>
                        <p class="category">taxa!</p>
                    </div>
                    <div class="card-content">
                        <div name="formTaxa">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="<?php echo e($taxa->nome); ?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Valor</label>
                                        <input id="valor" name="valor" type="text" class="form-control" disabled value="<?php echo e($taxa->valor); ?>">
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo e(url("taxa/edit/{$taxa->id}")); ?>" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="<?php echo e(url('taxa/index')); ?>" type="button" class="btn btn-primary pull-right">Voltar</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.layouts.model', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>