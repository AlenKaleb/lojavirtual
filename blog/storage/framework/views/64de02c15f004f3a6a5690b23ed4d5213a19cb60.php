<?php $__env->startSection('title'); ?>
    <h3>Edição de Cliente Físico</h3>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('action'); ?>
    <?php echo e(url("fisico/update/{$clienteFisico->id}")); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('panel.fisico._form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>