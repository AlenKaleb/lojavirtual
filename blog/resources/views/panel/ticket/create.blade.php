@extends('panel.ticket._form')
@section('title')
    <h3>Cadastro de Ticket de Desconto de Venda</h3>
@endsection
@section('action')
    {{ url("ticket/store") }}
@endsection