@extends('panel.layouts.model')
@section('content')
    <!-- Select2 -->
    <link href="{{ asset('panel/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">@yield('title')</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form name="formTicket" action="@yield('action')" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" value="{{ (!empty($ticket->nome)?$ticket->nome:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Codigo</label>
                                        <input id="codigo" name="codigo" type="text" class="form-control" value="{{ (!empty($ticket->codigo)?$ticket->codigo:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Desconto</label>
                                        <input id="desconto" name="desconto" type="text" class="form-control" value="{{ (!empty($ticket->desconto)?$ticket->desconto:null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{ csrf_field() }}
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection