@extends('panel.ticket._form')
@section('title')
    <h3>Edição de Ticket de Desconto de Venda</h3>
@endsection
@section('action')
    {{ url("ticket/update/{$ticket->id}") }}
@endsection