@extends('panel.layouts.model')
@section('content')

    <!-- Bootstrap TagsInput -->
    <link href="{{ asset('panel/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">

<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações do produto</h4>
                        <p class="category">produto!</p>
                    </div>
                    <div class="card-content">
                        <div name="formTaxa">
                            <div class="row">
                                @foreach($fotos as $key => $foto)
                                    <div class="col-sm-2">
                                        <h4>Foto {{ $key }}</h4>
                                        <img src="{{ url("painel/produtofotos/image/{$foto->nome_gerado}") }}" alt="Foto {{ $key }}" class="rounded img-fluid">
                                    </div>
                                @endforeach
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="togglebutton">
                                        <label>
                                            <input name="status" type="checkbox" {{ ($produto->status == 1?'checked=""':null) }} disabled value="1">
                                            Status
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="{{ (!empty($produto->nome)?$produto->nome:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Categoria</label>
                                        <select id="categoria_id" name="categoria_id" class="form-control select2-categoria" disabled>
                                            <option selected>Selecione uma categoria</option>
                                            @if(!empty($produto->categoria_id))
                                                <option selected>{{ $produto->nomeCategoria }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Qtd</label>
                                        <input id="qtd" name="qtd" type="text" class="form-control" disabled value="{{ (!empty($produto->qtd)?$produto->qtd:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Valor Unitário</label>
                                        <input id="valor_unitario" name="valor_unitario" type="text" class="form-control" disabled value="{{ (!empty($produto->valor_unitario)?$produto->valor_unitario:null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($taxas as $taxa)
                                    <div class="col-md-5">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Taxa</label>
                                            <select id="taxa" name="taxa_id[]" class="form-control select2-taxa" disabled>
                                                @if(!empty($taxa->id))
                                                    <option selected>{{ $taxa->nome }}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating btn-group" data-toggle="buttons">
                                        <label class="control-label">Compexidade</label>
                                        <label class="btn btn-default btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 1?'active':(empty($produto->complexidade)?'active':null)) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="1" {{ (!empty($produto->complexidade) && $produto->complexidade == 1?'checked':(empty($produto->complexidade)?'checked':null)) }}> &nbsp; 1 &nbsp;
                                        </label>
                                        <label class="btn btn-success btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 2?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="2" {{ (!empty($produto->complexidade) && $produto->complexidade == 2?'checked':null) }}> &nbsp; 2 &nbsp;
                                        </label>
                                        <label class="btn btn-primary btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 3?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="3" {{ (!empty($produto->complexidade) && $produto->complexidade == 3?'checked':null) }}> &nbsp; 3 &nbsp;
                                        </label>
                                        <label class="btn btn-warning btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 4?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="4" {{ (!empty($produto->complexidade) && $produto->complexidade == 4?'checked':null) }}> &nbsp; 4 &nbsp;
                                        </label>
                                        <label class="btn btn-danger btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 5?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default" disabled>
                                            <input type="radio" name="complexidade" value="5" {{ (!empty($produto->complexidade) && $produto->complexidade == 5?'checked':null) }}> &nbsp; 5 &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label">Tags de Tecnologias</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_tecnologia" name="tag_tecnologia" type="text" class="form-control" data-role="tagsinput" disabled value="{{ (!empty($produto->tag_tecnologia)?$produto->tag_tecnologia:null) }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Tags de Vantagens</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_vantagem" name="tag_vantagem" type="text" class="form-control" data-role="tagsinput" disabled value="{{ (!empty($produto->tag_vantagem)?$produto->tag_vantagem:null) }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Tags de Desvantagens</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_desvantagem" name="tag_desvantagem" type="text" class="form-control" data-role="tagsinput" disabled value="{{ (!empty($produto->tag_desvantagem)?$produto->tag_desvantagem:null) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Descrição</label>
                                        <div class="form-group label-floating">
                                            <label class="control-label"> Insira detalhes sobre o produto</label>
                                            <textarea name="descricao" class="form-control" rows="5" disabled>{{ (!empty($produto->descricao)?$produto->descricao:null) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url("produto/edit/{$produto->id}") }}" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="{{ url('produto/index') }}" type="button" class="btn btn-primary pull-right">Voltar</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--   Core JS Files   -->
    <script src="{{ asset('panel/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>

    <!-- Bootstrap TagsInput -->
    <script src="{{ asset('panel/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.js') }}"></script>
<script type="application/javascript">
    $(".bootstrap-tagsinput > input").prop("disabled", true);
</script>
@endsection