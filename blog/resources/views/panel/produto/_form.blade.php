@extends('panel.layouts.model')
@section('content')
    <!-- Select2 -->
    <link href="{{ asset('panel/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">

    <!-- Bootstrap TagsInput -->
    <link href="{{ asset('panel/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">@yield('title')</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form name="formProduto" action="@yield('action')" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="togglebutton">
                                        <label>
                                            <input name="status" type="checkbox" {{ (!empty($produto->status) && $produto->status == 1?'checked="true"':null) }} value="1">
                                            Status
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" value="{{ (!empty($produto->nome)?$produto->nome:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Categoria</label>
                                        <select id="categoria_id" name="categoria_id" class="form-control select2-categoria">
                                            <option selected>Selecione uma categoria</option>
                                            @if(!empty($produto->categoria_id))
                                                <option selected value="{{ $produto->categoria_id }}">{{ $produto->nomeCategoria }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Qtd</label>
                                        <input id="qtd" name="qtd" type="text" class="form-control" value="{{ (!empty($produto->qtd)?$produto->qtd:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Valor Unitário</label>
                                        <input id="valor_unitario" name="valor_unitario" type="text" class="form-control" value="{{ (!empty($produto->valor_unitario)?$produto->valor_unitario:null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="divTaxas">
                                    <div id="divTaxa">
                                        <div class="col-md-5">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Taxa</label>
                                                <select id="taxa" name="taxa_id[]" class="form-control select2-taxa">
                                                    <option selected>Selecione uma taxa</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button id="removerTaxa" type="button" class="btn btn-sm btn-primary btn-just-icon removerTaxa">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button id="novaTaxa" type="button" class="btn btn-success">Adicionar Taxa</button>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Fotos</label>
                                        <input id="imagem" name="imagem[]" type="file" multiple="" class="form-control" accept="image/*">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating btn-group" data-toggle="buttons">
                                        <label class="control-label">Compexidade</label>
                                        <label class="btn btn-default btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 1?'active':(empty($produto->complexidade)?'active':null)) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="complexidade" value="1" {{ (!empty($produto->complexidade) && $produto->complexidade == 1?'checked':(empty($produto->complexidade)?'checked':null)) }}> &nbsp; 1 &nbsp;
                                        </label>
                                        <label class="btn btn-success btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 2?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="complexidade" value="2" {{ (!empty($produto->complexidade) && $produto->complexidade == 2?'checked':null) }}> &nbsp; 2 &nbsp;
                                        </label>
                                        <label class="btn btn-primary btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 3?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="complexidade" value="3" {{ (!empty($produto->complexidade) && $produto->complexidade == 3?'checked':null) }}> &nbsp; 3 &nbsp;
                                        </label>
                                        <label class="btn btn-warning btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 4?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="complexidade" value="4" {{ (!empty($produto->complexidade) && $produto->complexidade == 4?'checked':null) }}> &nbsp; 4 &nbsp;
                                        </label>
                                        <label class="btn btn-danger btn-sm {{ (!empty($produto->complexidade) && $produto->complexidade == 5?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="complexidade" value="5" {{ (!empty($produto->complexidade) && $produto->complexidade == 5?'checked':null) }}> &nbsp; 5 &nbsp;
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label">Tags de Tecnologias</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_tecnologia" name="tag_tecnologia" type="text" class="form-control" data-role="tagsinput" value="{{ (!empty($produto->tag_tecnologia)?$produto->tag_tecnologia:null) }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Tags de Vantagens</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_vantagem" name="tag_vantagem" type="text" class="form-control" data-role="tagsinput" value="{{ (!empty($produto->tag_vantagem)?$produto->tag_vantagem:null) }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label class="control-label">Tags de Desvantagens</label>
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <input id="tag_desvantagem" name="tag_desvantagem" type="text" class="form-control" data-role="tagsinput" value="{{ (!empty($produto->tag_desvantagem)?$produto->tag_desvantagem:null) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Descrição</label>
                                        <div class="form-group label-floating">
                                            <label class="control-label"> Insira detalhes sobre o produto</label>
                                            <textarea name="descricao" class="form-control" rows="5">{{ (!empty($produto->descricao)?$produto->descricao:null) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{ csrf_field() }}
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--   Core JS Files   -->
    <script src="{{ asset('panel/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>

    <!-- Select2 -->
    <script src="{{ asset('panel/assets/vendors/select2/dist/js/select2.min.js') }}"></script>

    <!-- Bootstrap TagsInput -->
    <script src="{{ asset('panel/assets/vendors/bootstrap-tagsinput/dist/bootstrap-tagsinput.js') }}"></script>

    <script type="application/javascript">
        $(".select2-categoria").select2({
            tags: false,
            multiple: false,
            minimumInputLength: 2,
            ajax: {
                url: "{{ url('categoria/pesquisarCategoria') }}",
                dataType: "json",
                type: "GET",
                data: function (params) {

                    var queryParameters = {
                        term: params.term
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.nome,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
        jQuery(function ($) {
            $("#divTaxa").prop('disabled',true).hide();
            var count = 0;
            @if(!empty($produto->id))
                @foreach($taxas as $taxa)
                    adicionarTaxa(parseInt("{{ $taxa->id }}"),"{{ $taxa->nome }}");
            @endforeach
        @endif
        function adicionarTaxa(taxa_id,nome){
                var div = $("#divTaxa").clone().prop('disabed',false).show();
                div.removeAttr('id').attr('id', "divTaxa" + count);
                div.find("select[id='taxa']").removeAttr('id').removeAttr('name').attr('id', 'taxa' + count).attr('name', "taxa_id[" + count + "]").html('<option selected value="'+taxa_id+'">'+nome+'</option>');
                div.find("button[id='removerTaxa']").removeAttr('id').attr('id', 'removerTaxa-' + count);
                div.appendTo('#divTaxas');

                $('#taxa' + count).select2({
                    placeholder: 'Selecione uma taxa',
                    tags: false,
                    multiple: false,
                    minimumInputLength: 2,
                    ajax: {
                        url: "{{ url('taxa/pesquisarTaxa') }}",
                        dataType: "json",
                        type: "GET",
                        data: function (params) {

                            var queryParameters = {
                                term: params.term
                            }
                            return queryParameters;
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.nome,
                                        id: item.id
                                    }
                                })
                            };
                        }
                    }
                });

                count += 1;

                $(".removerTaxa").click(function () {
                    var posicao = this.id.split('-');
                    removerTaxa(posicao[1]);
                });
            }

            function removerTaxa(count){
                $("#divTaxa"+count).remove();
            }

            $("#novaTaxa").click(function () {
                adicionarTaxa('','');
            });
        });
    </script>
@endsection