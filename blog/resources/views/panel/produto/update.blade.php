@extends('panel.produto._form')
@section('title')
    <h3>Edição de Produto</h3>
@endsection
@section('action')
    {{ url("produto/update/{$produto->id}") }}
@endsection