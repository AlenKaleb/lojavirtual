@extends('panel.produto._form')
@section('title')
    <h3>Cadastro de Produto</h3>
@endsection
@section('action')
    {{ url("produto/store") }}
@endsection