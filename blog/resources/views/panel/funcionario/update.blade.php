@extends('panel.funcionario._form')
@section('title')
    <h3>Edição de Funcionário</h3>
@endsection
@section('action')
    {{ url("funcionario/update/{$funcionario->id}") }}
@endsection