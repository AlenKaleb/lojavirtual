@extends('panel.funcionario._form')
@section('title')
    <h3>Cadastro de Funcionário</h3>
@endsection
@section('action')
    {{ url("funcionario/store") }}
@endsection