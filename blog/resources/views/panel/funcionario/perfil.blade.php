@extends('layouts.model')
@section('content')
<?php
    $dirImagemPerfil = session('objectUsuario')->imagem->nome_gerado;
?>
    <!-- page content -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Perfil -  {{ session('nome') }}</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                        <div class="profile_img">
                            <div id="crop-avatar">
                                <!-- Current avatar -->
                                <img class="img-responsive avatar-view" src="{{ url("painel/fotosperfil/image/{$dirImagemPerfil}")  }}" alt="Avatar" title="Change the avatar">
                            </div>
                        </div>
                        <h3>{{ session('nome') }}</h3>

                        <ul class="list-unstyled user_data">
                            <li><i class="fa fa-archive user-profile-icon"></i> {{ session('objectUsuario')->pessoa->cpf }}
                            </li>
                            <li><i class="fa fa-mail-forward user-profile-icon"></i> {{ session('objectUsuario')->email }}
                            </li>
                        </ul>
                        <br />
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Chats</a>
                                </li>
                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Projetos</a>
                                </li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                    <!-- start user projects -->
                                    <table class="data table table-striped no-margin">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Chat</th>
                                            <th>Criado</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($chats as $chat)
                                            <tr>
                                                <td>{{ $chat->id }}</td>
                                                <td>{{ $chat->nome }}</td>
                                                <td>{{ $chat->created_at }}</td>
                                                <td>{{ ($chat->status == 0?'Encerrado':'Aberto') }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <!-- end user projects -->
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                                    <!-- start user projects -->
                                    <table class="data table table-striped no-margin">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Projeto</th>
                                            <th>Data / Hora de Inicio</th>
                                            <th>Data / Hora de Termino</th>
                                            <th>Complexidade</th>
                                            <th>Preço</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($projetos as $projeto)
                                            <tr>
                                                <td>{{ $projeto->id }}</td>
                                                <td>{{ $projeto->nome }}</td>
                                                <td>{{ $projeto->data_hora_inicio }}</td>
                                                <td>{{ $projeto->data_hora_termino }}</td>
                                                <td>{{ $projeto->complexidade }}</td>
                                                <td>{{ $projeto->preco }}</td>
                                                <td>{{ ($projeto->status == 0?'Encerrado':'Aberto') }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <!-- end user projects -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /page content -->
@endsection