<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('panel/assets/img/apple-icon.png') }}" />
    <link rel="icon" type="image/png" href="{{ url('panel/assets/img/favicon.png') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>AWKA Technologic</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{ url('panel/assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ url('panel/assets/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ url('panel/assets/css/demo.css') }}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="{{ url('panel/assets/img/sidebar-1.jpg') }}">
        <!--
    Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

    Tip 2: you can also add an image using data-image tag
-->
        <div class="logo">
            <a href="http://www.creative-tim.com" class="simple-text">
                AWKA Technologic
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="activePage">
                    <a href="{{ url('') }}">
                        <i class="material-icons">dashboard</i>
                        <p>Inicio</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('funcionario/index') }}">
                        <i class="material-icons">person</i>
                        <p>Funcionarios</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('fisico/index') }}">
                        <i class="material-icons">person</i>
                        <p>Cliente Físico</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('juridico/index') }}">
                        <i class="material-icons">person</i>
                        <p>Cliente Jurídico</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('categoria/index') }}">
                        <i class="material-icons">content_paste</i>
                        <p>Categorias</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('formapagamento/index') }}">
                        <i class="material-icons">library_books</i>
                        <p>Forma de Pagamento</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('taxa/index') }}">
                        <i class="material-icons">content_paste</i>
                        <p>Taxas</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('produto/index') }}">
                        <i class="material-icons">content_paste</i>
                        <p>Produtos</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('ticket/index') }}">
                        <i class="material-icons">library_books</i>
                        <p>Tickets</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('carrinho/index') }}">
                        <i class="material-icons">library_books</i>
                        <p>Carrinho</p>
                    </a>
                </li>
                <li class="activePage">
                    <a href="{{ url('venda/index') }}">
                        <i class="material-icons">library_books</i>
                        <p>Venda</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> AWKA Technologic </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">dashboard</i>
                                <p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">notifications</i>
                                <span class="notification">5</span>
                                <p class="hidden-lg hidden-md">Notifications</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">Mike John responded to your email</a>
                                </li>
                                <li>
                                    <a href="#">You have 5 new tasks</a>
                                </li>
                                <li>
                                    <a href="#">You're now friend with Andrew</a>
                                </li>
                                <li>
                                    <a href="#">Another Notification</a>
                                </li>
                                <li>
                                    <a href="#">Another One</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">person</i>
                                <p class="hidden-lg hidden-md">Profile</p>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#">Perfil</a>
                                </li>
                                <li>
                                    <a href="{{ url('logout') }}">Deslogar</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group  is-empty">
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="material-input"></span>
                        </div>
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </form>
                </div>
            </div>
        </nav>
        <div class="content">
            @yield('content')
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <p class="copyright pull-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    <a href="#awkatechnologic">AWKA Technologic</a> Inovação em Softwares
                </p>
            </div>
        </footer>
    </div>
</div>
</body>
<!--   Core JS Files   -->
<script src="{{ asset('panel/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('panel/assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('panel/assets/js/material.min.js') }}" type="text/javascript"></script>
<!--  Charts Plugin -->
{{--<script src="{{ asset('panel/assets/js/chartist.min.js') }}"></script>--}}
<!--  Dynamic Elements plugin -->
<script src="{{ asset('panel/assets/js/arrive.min.js') }}"></script>
<!--  PerfectScrollbar Library -->
<script src="{{ asset('panel/assets/js/perfect-scrollbar.jquery.min.js') }}"></script>
<!--  Notifications Plugin    -->
<script src="{{ asset('panel/assets/js/bootstrap-notify.js') }}"></script>
<!--  Google Maps Plugin    -->
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--}}
<!-- Material Dashboard javascript methods -->
<script src="{{ asset('panel/assets/js/material-dashboard.js?v=1.2.0') }}"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="{{ asset('panel/assets/js/demo.js') }}"></script>
<script type="text/javascript">
//    $(document).ready(function() {
//
//        // Javascript method's body can be found in assets/js/demos.js
//        demo.initDashboardPageCharts();
//
//    });
</script>

<script type="application/javascript">
    $(document).ready(function () {
        var link = window.location.href;
        link = link.split('?');
        $(".activePage a").each(function () {
            if($(this).attr('href') == $('a[href="' + link[0] + '"]').attr('href')){
                $(this).closest("li").addClass('active').parent().parent('li').addClass('open active').parent().parent().addClass('open active');
            }
        });
    });
</script>

</html>