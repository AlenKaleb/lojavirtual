@extends('panel.fisico._form')
@section('title')
    <h3>Edição de Cliente Físico</h3>
@endsection
@section('action')
    {{ url("fisico/update/{$clienteFisico->id}") }}
@endsection