@extends('panel.layouts.model')
@section('content')
    <!-- Select2 -->
    <link href="{{ asset('panel/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">

    <!-- DateTimePicker -->
    <link href="{{ asset('panel/assets/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">@yield('title')</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form name="formFisico" action="@yield('action')" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" value="{{ (!empty($clienteFisico->nome)?$clienteFisico->nome:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">CPF</label>
                                        <input id="cpf" name="cpf" type="text" class="form-control" value="{{ (!empty($clienteFisico->cpf)?$clienteFisico->cpf:null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="radio">
                                        <label>
                                            <input id="masculino" type="radio" name="sexo" value="M" {{ (!empty($clienteFisico->sexo) && $clienteFisico->sexo == 'M'?'checked="true"':'checked="true"') }}>
                                            Masculino
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input id="feminino" type="radio" name="sexo" value="F" {{ (!empty($clienteFisico->sexo) && $clienteFisico->sexo == 'F'?'checked="true"':null) }}>
                                            Feminino
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">RG</label>
                                        <input id="rg" name="rg" type="text" class="form-control" value="{{ (!empty($clienteFisico->rg)?$clienteFisico->rg:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Data de Nascimento</label>
                                        <input id="data_nascimento" name="data_nascimento" type="text" class="form-control datepicker" value="{{ (!empty($clienteFisico->data_nascimento)?$clienteFisico->data_nascimento:null) }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div id="divTelefones">
                                    <div id="divTelefone">
                                        <div class="col-md-5">
                                            <div class="form-group label-floating">
                                                <label class="control-label">Telefone</label>
                                                <input id="telefone" name="telefone[]" type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button id="removerTelefone" type="button" class="btn btn-primary btn-just-icon removerTelefone">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button id="novoTelefone" type="button" class="btn btn-success">Adicionar Telefone</button>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cep</label>
                                        <input id="cep" name="cep" type="text" class="form-control" value="{{ (!empty($endereco->cep)?$endereco->cep:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pais</label>
                                        <select id="pais" name="pais_id" class="form-control select2-pais">
                                            <option selected>Selecione um país</option>
                                            @if(!empty($paises[0]))
                                                @foreach($paises as $pais)
                                                    @if(!empty($endereco->pais_id) && $pais->id == $endereco->pais_id)
                                                        <option selected value="{{ $pais->id }}">{{ $pais->nome_pt }}</option>
                                                    @else
                                                        <option value="{{ $pais->id }}">{{ $pais->nome_pt }}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Estado</label>
                                        <select id="estado" name="estado_id" class="form-control select2-estado">
                                            <option selected>Selecione um estado</option>
                                            @if(!empty($endereco->estado_id))
                                                <option selected value="{{ $endereco->estado_id }}">{{ $endereco->nomeEstado }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cidade</label>
                                        <select id="cidade" name="cidade_id" class="form-control select2-cidade">
                                            <option selected>Selecione uma cidade</option>
                                            @if(!empty($endereco->cidade_id))
                                                <option selected value="{{ $endereco->cidade_id }}">{{ $endereco->nomeCidade }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Logradouro</label>
                                        <input id="logradouro" name="logradouro" type="text" class="form-control" value="{{ (!empty($endereco->logradouro)?$endereco->logradouro:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Número</label>
                                        <input id="numero" name="numero" type="text" class="form-control" value="{{ (!empty($endereco->numero)?$endereco->numero:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Bairro</label>
                                        <input id="bairro" name="bairro" type="text" class="form-control" value="{{ (!empty($endereco->bairro)?$endereco->bairro:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Complemento</label>
                                        <input id="complemento" name="complemento" type="text" class="form-control" value="{{ (!empty($endereco->complemento)?$endereco->complemento:null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input name="email" type="email" class="form-control" value="{{ (!empty($clienteFisico->email)?$clienteFisico->email:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Senha</label>
                                        <input id="password" name="password" type="password" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Confirmar Senha</label>
                                        <input id="confirmarPassword" name="confimarPassword" type="password" class="form-control">
                                    </div>
                                </div>
                                {{ csrf_field() }}
                            </div>
                            <input id="user_id" name="user_id" type="hidden" class="form-control" value="{{ (!empty($clienteFisico->user_id)?$clienteFisico->user_id:null) }}">
                            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--   Core JS Files   -->
<script src="{{ asset('panel/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>

<!-- Select2 -->
<script src="{{ asset('panel/assets/vendors/select2/dist/js/select2.min.js') }}"></script>

<!-- bootstrap-daterangepicker -->
<script src="{{ asset('panel/assets/vendors/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/moment/locale/pt-br.js') }}"></script>


<!-- DateTimePicker -->
<script src="{{ asset('panel/assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="application/javascript">

    $('.datepicker').datetimepicker({
        format: 'DD/MM/YYYY'
    });

    $("#cep").change(function () {
        var cep_code = $(this).val();
        if (cep_code.length <= 0)
            return;
        $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", {code: cep_code},
                function (result) {
                    if (result.status != 1) {
                        alert(result.message || "Houve um erro desconhecido");
                        return;
                    }
                    $("input#cep").val(result.code);
                    $("input#bairro").val(result.district);
                    $("input#logradouro").val(result.address);
                });
    });
    $(".select2-pais").select2({
        tags: false,
        multiple: false,
        minimumInputLength: 2,
        ajax: {
            url: "{{ url('pais/pesquisarPais') }}",
            dataType: "json",
            type: "GET",
            data: function (params) {

                var queryParameters = {
                    term: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $(".select2-estado").select2({
        tags: false,
        multiple: false,
        minimumInputLength: 2,
        ajax: {
            url: "{{ url('estado/pesquisarEstado') }}",
            dataType: "json",
            type: "GET",
            data: function (params) {

                var queryParameters = {
                    term: params.term,
                    pais_id: $("#pais").val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    $(".select2-cidade").select2({
        tags: false,
        multiple: false,
        minimumInputLength: 2,
        ajax: {
            url: "{{ url('cidade/pesquisarCidade') }}",
            dataType: "json",
            type: "GET",
            data: function (params) {

                var queryParameters = {
                    term: params.term,
                    estado_id: $("#estado").val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nome,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

    jQuery(function ($) {
        $("#divTelefone").prop('disabled',true).hide();
        var count = 0;
        @if(!empty($clienteFisico->id))
            @foreach($telefones as $telefone)
                    adicionarTelefone("{{ $telefone->telefone }}");
        @endforeach
    @endif
    function adicionarTelefone(telefone){
            var div = $("#divTelefone").clone().prop('disabed',false).show();
            div.removeAttr('id').attr('id', "divTelefone" + count);
            div.find("input[id='telefone']").removeAttr('id').removeAttr('name').attr('id', 'telefone' + count).attr('name', "telefone[" + count + "]").val(telefone);
            div.find("button[id='removerTelefone']").removeAttr('id').attr('id', 'removerTelefone-' + count);
            div.appendTo('#divTelefones');

            count += 1;

            $(".removerTelefone").click(function () {
                var posicao = this.id.split('-');
                removerTelefone(posicao[1]);
            });
        }

        function removerTelefone(count){
            $("#divTelefone"+count).remove();
        }

        $("#novoTelefone").click(function () {
            adicionarTelefone('');
        });
    });
</script>
@endsection