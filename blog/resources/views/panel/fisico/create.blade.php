@extends('panel.fisico._form')
@section('title')
    <h3>Cadastro de Cliente Físico</h3>
@endsection
@section('action')
    {{ url("fisico/store") }}
@endsection