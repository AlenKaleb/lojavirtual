@extends('panel.layouts.model')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações Pessoais</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        <div name="formJuridico">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Razão Social</label>
                                        <input id="razao_social" name="razao_social" type="text" class="form-control" disabled value="{{ (!empty($clienteJuridico->razao_social)?$clienteJuridico->razao_social:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome Fantasia</label>
                                        <input id="nome_fantasia" name="nome_fantasia" type="text" class="form-control" disabled value="{{ (!empty($clienteJuridico->nome_fantasia)?$clienteJuridico->nome_fantasia:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">CNPJ</label>
                                        <input id="cnpj" name="cnpj" type="text" class="form-control" disabled value="{{ (!empty($clienteJuridico->cnpj)?$clienteJuridico->cnpj:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Data de Registro</label>
                                        <input id="data_registro" name="data_registro" type="text" class="form-control datepicker" disabled value="{{ (!empty($clienteJuridico->data_registro)?$clienteJuridico->data_registro:null) }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Inscrição Estadual</label>
                                        <input id="inscricao_estadual" name="inscricao_estadual" type="text" class="form-control" disabled value="{{ (!empty($clienteJuridico->inscricao_estadual)?$clienteJuridico->inscricao_estadual:null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @if(!empty($telefones[0]))
                                    @foreach($telefones as $telefone)
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Telefone</label>
                                                    <input id="telefone" type="text" class="form-control" disabled value="{{ $telefone->telefone }}">
                                                </div>
                                            </div>
                                    @endforeach
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cep</label>
                                        <input id="cep" name="cep" type="text" class="form-control" disabled value="{{ $endereco->cep }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Pais</label>
                                        <select id="pais" name="pais_id" class="form-control select2-pais" disabled>
                                           @if(!empty($endereco->pais_id))
                                                <option selected value="{{ $endereco->pais_id }}">{{ $endereco->nomePais }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Estado</label>
                                        <select id="estado" name="estado_id" class="form-control select2-estado" disabled>
                                            @if(!empty($endereco->estado_id))
                                                <option selected value="{{ $endereco->estado_id }}">{{ $endereco->nomeEstado }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Cidade</label>
                                        <select id="cidade" name="cidade_id" class="form-control select2-cidade" disabled>
                                            @if(!empty($endereco->cidade_id))
                                                <option selected value="{{ $endereco->cidade_id }}">{{ $endereco->nomeCidade }}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Logradouro</label>
                                        <input id="logradouro" name="logradouro" type="text" class="form-control" disabled value="{{ $endereco->logradouro }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Número</label>
                                        <input id="numero" name="numero" type="text" class="form-control" disabled value="{{ $endereco->numero }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Bairro</label>
                                        <input id="bairro" name="bairro" type="text" class="form-control" disabled value="{{ $endereco->bairro }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Complemento</label>
                                        <input id="complemento" name="complemento" type="text" class="form-control" disabled value="{{ $endereco->complemento }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Email</label>
                                        <input name="email" type="email" class="form-control" disabled value="{{ (!empty($clienteJuridico->email)?$clienteJuridico->email:null) }}">
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url("juridico/edit/{$clienteJuridico->id}") }}" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="{{ url('juridico/index') }}" type="button" class="btn btn-primary pull-right">Voltar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection