@extends('panel.juridico._form')
@section('title')
    <h3>Edição de Cliente Jurídico</h3>
@endsection
@section('action')
    {{ url("juridico/update/{$clienteJuridico->id}") }}
@endsection