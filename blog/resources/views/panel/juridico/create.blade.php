@extends('panel.juridico._form')
@section('title')
    <h3>Cadastro de Cliente Jurídico</h3>
@endsection
@section('action')
    {{ url("juridico/store") }}
@endsection