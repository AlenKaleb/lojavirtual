@extends('layouts.model')
@section('content')
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>@yield('title')</h3>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Formulário de cadastro de usuário</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="@yield('action')" method="post" class="form-horizontal form-label-left" enctype="multipart/form-data" novalidate>

                            <p>Insira os dados do usuário
                            </p>
                            <span class="section">Informações</span>

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nome <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="nome" class="form-control col-md-7 col-xs-12" name="nome" placeholder="Infome o nome" required="required" type="text" value="{{ (!empty($pessoa->nome)?$pessoa->nome:null) }}">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Cpf <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="cpf" name="cpf" required="required" class="form-control col-md-7 col-xs-12" placeholder="Infome o cpf" value="{{ (!empty($pessoa->cpf)?$pessoa->cpf:null) }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Sexo</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="gender" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default {{ (!empty($pessoa->sexo) && $pessoa->sexo == 'M'?'active':(empty($pessoa->sexo)?'active':null)) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="sexo" value="M" {{ (!empty($pessoa->sexo) && $pessoa->sexo == 'M'?'checked':(empty($pessoa->sexo)?'checked':null)) }}> &nbsp; Masculino &nbsp;
                                        </label>
                                        <label class="btn btn-primary {{ (!empty($pessoa->sexo) && $pessoa->sexo == 'F'?'active':null) }}" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="sexo" value="F" {{ (!empty($pessoa->sexo) && $pessoa->sexo == 'F'?'checked':null) }}> Feminino
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="arquivo">Foto de Perfil <span class="required">*</span>
                                </label>
                                <div class="col-md-5 col-sm-5 col-xs-12">
                                    <input id="imagem" class="form-control col-md-7 col-xs-12" name="imagem" placeholder="Infome a imagem" {{ (empty($pessoa->id)?'required="required"':null) }} type="file" value="">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" class="form-control has-feedback-left" id="email" name="email" placeholder="Email" required="required" value="{{ (!empty($pessoa->email)?$pessoa->email:null) }}">
                                    <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                </div>
                            </div>

                            <div class="item form-group">
                                <label for="password" class="control-label col-md-3">Senha</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="password" type="password" name="password" class="form-control col-md-7 col-xs-12" {{ (empty($pessoa->id)?'required="required"':null) }}>
                                </div>
                            </div>
                            <div class="item form-group">
                                <label for="password2" class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar Senha</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="password2" type="password" name="ConfirmarPassword" data-validate-linked="password" class="form-control col-md-7 col-xs-12" {{ (empty($pessoa->id)?'required="required"':null) }}>
                                </div>
                            </div>

                            <!-- Start to do list -->
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Permissões de Acesso</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <ul class="to_do">
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="U0" {{ (!empty($pessoa->usuario->permissaoAcessos[0]) && $pessoa->usuario->permissaoAcessos[0]->cadastrar == 1?"checked":null) }}> Cadastrar Usuário </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="U1" {{ (!empty($pessoa->usuario->permissaoAcessos[0]) && $pessoa->usuario->permissaoAcessos[0]->listar == 1?"checked":null) }}> Listar Usuário</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="U2" {{ (!empty($pessoa->usuario->permissaoAcessos[0]) && $pessoa->usuario->permissaoAcessos[0]->alterar == 1?"checked":null) }}> Alterar Usuário</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="U3" {{ (!empty($pessoa->usuario->permissaoAcessos[0]) && $pessoa->usuario->permissaoAcessos[0]->excluir == 1?"checked":null) }}> Excluir Usuário</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="K0" {{ (!empty($pessoa->usuario->permissaoAcessos[1]) && $pessoa->usuario->permissaoAcessos[1]->cadastrar == 1?"checked":null) }}> Cadastrar Cliente Físico / Jurídico</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="K1" {{ (!empty($pessoa->usuario->permissaoAcessos[1]) && $pessoa->usuario->permissaoAcessos[1]->listar == 1?"checked":null) }}> Listar Cliente Físico / Jurídico</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="K2" {{ (!empty($pessoa->usuario->permissaoAcessos[1]) && $pessoa->usuario->permissaoAcessos[1]->alterar == 1?"checked":null) }}> Alterar Cliente Físico / Jurídico</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="K3" {{ (!empty($pessoa->usuario->permissaoAcessos[1]) && $pessoa->usuario->permissaoAcessos[1]->excluir == 1?"checked":null) }}> Excluir Cliente Físico / Jurídico</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="I0" {{ (!empty($pessoa->usuario->permissaoAcessos[2]) && $pessoa->usuario->permissaoAcessos[2]->cadastrar == 1?"checked":null) }}> Cadastrar Integrante</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="I1" {{ (!empty($pessoa->usuario->permissaoAcessos[2]) && $pessoa->usuario->permissaoAcessos[2]->listar == 1?"checked":null) }}> Listar Integrante</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="I2" {{ (!empty($pessoa->usuario->permissaoAcessos[2]) && $pessoa->usuario->permissaoAcessos[2]->alterar == 1?"checked":null) }}> Alterar Integrante </p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="I3" {{ (!empty($pessoa->usuario->permissaoAcessos[2]) && $pessoa->usuario->permissaoAcessos[2]->excluir == 1?"checked":null) }}> Excluir Integrante</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="C0" {{ (!empty($pessoa->usuario->permissaoAcessos[3]) && $pessoa->usuario->permissaoAcessos[3]->cadastrar == 1?"checked":null) }}> Cadastrar Chat</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="C1" {{ (!empty($pessoa->usuario->permissaoAcessos[3]) && $pessoa->usuario->permissaoAcessos[3]->listar == 1?"checked":null) }}> Listar Chat</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="C2" {{ (!empty($pessoa->usuario->permissaoAcessos[3]) && $pessoa->usuario->permissaoAcessos[3]->alterar == 1?"checked":null) }}> Alterar Chat</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="C3" {{ (!empty($pessoa->usuario->permissaoAcessos[3]) && $pessoa->usuario->permissaoAcessos[3]->excluir == 1?"checked":null) }}> Excluir Chat</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="S0" {{ (!empty($pessoa->usuario->permissaoAcessos[4]) && $pessoa->usuario->permissaoAcessos[4]->cadastrar == 1?"checked":null) }}> Cadastrar Conversa no Chat</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="S1" {{ (!empty($pessoa->usuario->permissaoAcessos[4]) && $pessoa->usuario->permissaoAcessos[4]->listar == 1?"checked":null) }}> Listar Conversa no Chat</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="S2" {{ (!empty($pessoa->usuario->permissaoAcessos[4]) && $pessoa->usuario->permissaoAcessos[4]->alterar == 1?"checked":null) }}> Alterar Conversa no Chat</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="S3" {{ (!empty($pessoa->usuario->permissaoAcessos[4]) && $pessoa->usuario->permissaoAcessos[4]->excluir == 1?"checked":null) }}> Excluir Chat</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <ul class="to_do">
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="A0" {{ (!empty($pessoa->usuario->permissaoAcessos[5]) && $pessoa->usuario->permissaoAcessos[5]->cadastrar == 1?"checked":null) }}> Cadastrar Arquivo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="A1" {{ (!empty($pessoa->usuario->permissaoAcessos[5]) && $pessoa->usuario->permissaoAcessos[5]->listar == 1?"checked":null) }}> Listar Arquivo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="A2" {{ (!empty($pessoa->usuario->permissaoAcessos[5]) && $pessoa->usuario->permissaoAcessos[5]->alterar == 1?"checked":null) }}> Alterar Arquivo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="A3" {{ (!empty($pessoa->usuario->permissaoAcessos[5]) && $pessoa->usuario->permissaoAcessos[5]->excluir == 1?"checked":null) }}> Excluir Arquivo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="P0" {{ (!empty($pessoa->usuario->permissaoAcessos[6]) && $pessoa->usuario->permissaoAcessos[6]->cadastrar == 1?"checked":null) }}> Cadastrar Porte</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="P1" {{ (!empty($pessoa->usuario->permissaoAcessos[6]) && $pessoa->usuario->permissaoAcessos[6]->listar == 1?"checked":null) }}> Listar Porte</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="P2" {{ (!empty($pessoa->usuario->permissaoAcessos[6]) && $pessoa->usuario->permissaoAcessos[6]->alterar == 1?"checked":null) }}> Alterar Porte</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="P3" {{ (!empty($pessoa->usuario->permissaoAcessos[6]) && $pessoa->usuario->permissaoAcessos[6]->excluir == 1?"checked":null) }}> Excluir Porte</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="D0" {{ (!empty($pessoa->usuario->permissaoAcessos[7]) && $pessoa->usuario->permissaoAcessos[7]->cadastrar == 1?"checked":null) }}> Cadastrar Papel do Integrante</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="D1" {{ (!empty($pessoa->usuario->permissaoAcessos[7]) && $pessoa->usuario->permissaoAcessos[7]->listar == 1?"checked":null) }}> Listar Papel do Integrante</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="D2" {{ (!empty($pessoa->usuario->permissaoAcessos[7]) && $pessoa->usuario->permissaoAcessos[7]->alterar == 1?"checked":null) }}> Alterar Papel do Integrante</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="D3" {{ (!empty($pessoa->usuario->permissaoAcessos[7]) && $pessoa->usuario->permissaoAcessos[7]->excluir == 1?"checked":null) }}> Excluir Papel do Integrante</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="E0" {{ (!empty($pessoa->usuario->permissaoAcessos[8]) && $pessoa->usuario->permissaoAcessos[8]->cadastrar == 1?"checked":null) }}> Cadastrar Etapa</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="E1" {{ (!empty($pessoa->usuario->permissaoAcessos[8]) && $pessoa->usuario->permissaoAcessos[8]->listar == 1?"checked":null) }}> Listar Etapa</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="E2" {{ (!empty($pessoa->usuario->permissaoAcessos[8]) && $pessoa->usuario->permissaoAcessos[8]->alterar == 1?"checked":null) }}> Alterar Etapa</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="E3" {{ (!empty($pessoa->usuario->permissaoAcessos[8]) && $pessoa->usuario->permissaoAcessos[8]->excluir == 1?"checked":null) }}> Excluir Etapa</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="J0" {{ (!empty($pessoa->usuario->permissaoAcessos[9]) && $pessoa->usuario->permissaoAcessos[9]->cadastrar == 1?"checked":null) }}> Cadastrar Projeto</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="J1" {{ (!empty($pessoa->usuario->permissaoAcessos[9]) && $pessoa->usuario->permissaoAcessos[9]->listar == 1?"checked":null) }}> Listar Projeto</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="J2" {{ (!empty($pessoa->usuario->permissaoAcessos[9]) && $pessoa->usuario->permissaoAcessos[9]->alterar == 1?"checked":null) }}> Alterar Projeto</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="J3" {{ (!empty($pessoa->usuario->permissaoAcessos[9]) && $pessoa->usuario->permissaoAcessos[9]->excluir == 1?"checked":null) }}> Excluir Projeto</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <ul class="to_do">
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="M0" {{ (!empty($pessoa->usuario->permissaoAcessos[10]) && $pessoa->usuario->permissaoAcessos[10]->cadastrar == 1?"checked":null) }}> Cadastrar Modulo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="M1" {{ (!empty($pessoa->usuario->permissaoAcessos[10]) && $pessoa->usuario->permissaoAcessos[10]->listar == 1?"checked":null) }}> Listar Modulo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="M2" {{ (!empty($pessoa->usuario->permissaoAcessos[10]) && $pessoa->usuario->permissaoAcessos[10]->alterar == 1?"checked":null) }}> Alterar Modulo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="M3" {{ (!empty($pessoa->usuario->permissaoAcessos[10]) && $pessoa->usuario->permissaoAcessos[10]->excluir == 1?"checked":null) }}> Excluir Modulo</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="Q0" {{ (!empty($pessoa->usuario->permissaoAcessos[11]) && $pessoa->usuario->permissaoAcessos[11]->cadastrar == 1?"checked":null) }}> Cadastrar Equipe</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="Q1" {{ (!empty($pessoa->usuario->permissaoAcessos[11]) && $pessoa->usuario->permissaoAcessos[11]->listar == 1?"checked":null) }}> Listar Equipe</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="Q2" {{ (!empty($pessoa->usuario->permissaoAcessos[11]) && $pessoa->usuario->permissaoAcessos[11]->alterar == 1?"checked":null) }}> Alterar Equipe</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="Q3" {{ (!empty($pessoa->usuario->permissaoAcessos[11]) && $pessoa->usuario->permissaoAcessos[11]->excluir == 1?"checked":null) }}> Excluir Equipe</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="V0" {{ (!empty($pessoa->usuario->permissaoAcessos[12]) && $pessoa->usuario->permissaoAcessos[12]->cadastrar == 1?"checked":null) }}> Cadastrar Atividade</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="V1" {{ (!empty($pessoa->usuario->permissaoAcessos[12]) && $pessoa->usuario->permissaoAcessos[12]->listar == 1?"checked":null) }}> Listar Atividade</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="V2" {{ (!empty($pessoa->usuario->permissaoAcessos[12]) && $pessoa->usuario->permissaoAcessos[12]->alterar == 1?"checked":null) }}> Alterar Atividade</p>
                                                </li>
                                                <li>
                                                    <p>
                                                        <input type="checkbox" class="flat" name="permissao[]" value="V3" {{ (!empty($pessoa->usuario->permissaoAcessos[12]) && $pessoa->usuario->permissaoAcessos[12]->excluir == 1?"checked":null) }}> Excluir Atividade</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End to do list -->

                            <div class="ln_solid"></div>
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <input type="hidden" class="form-control has-feedback-left" id="usuario_id" name="usuario_id" placeholder="Usuario_ID" value="{{ (!empty($pessoa->usuario_id)?$pessoa->usuario_id:null) }}">
                                    <a href="/usuario/index"><button id="cancelamento" type="button" class="btn btn-primary">Voltar</button></a>
                                    <button id="send" type="submit" class="btn btn-success">Gravar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection