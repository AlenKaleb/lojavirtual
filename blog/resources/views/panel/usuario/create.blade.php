@extends('usuario._form')
@section('title')
    <h3>Cadastro de Usuário</h3>
@endsection
@section('action')
    {{ url("usuario/store") }}
@endsection