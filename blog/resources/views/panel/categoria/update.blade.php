@extends('panel.categoria._form')
@section('title')
    <h3>Edição de Categoria de Produto</h3>
@endsection
@section('action')
    {{ url("categoria/update/{$categoria->id}") }}
@endsection