@extends('panel.categoria._form')
@section('title')
    <h3>Cadastro de Categoria de Produto</h3>
@endsection
@section('action')
    {{ url("categoria/store") }}
@endsection