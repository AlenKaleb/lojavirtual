@extends('panel.layouts.model')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações da categoria</h4>
                        <p class="category">categoria!</p>
                    </div>
                    <div class="card-content">
                        <div name="formCategoria">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="{{ $categoria->nome }}">
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url("categoria/edit/{$categoria->id}") }}" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="{{ url('categoria/index') }}" type="button" class="btn btn-primary pull-right">Voltar</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection