@extends('panel.taxa._form')
@section('title')
    <h3>Edição de Taxa de Produto</h3>
@endsection
@section('action')
    {{ url("taxa/update/{$taxa->id}") }}
@endsection