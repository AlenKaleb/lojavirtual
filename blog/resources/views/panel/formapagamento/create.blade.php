@extends('panel.formapagamento._form')
@section('title')
    <h3>Cadastro de Forma de Pagamento</h3>
@endsection
@section('action')
    {{ url("formapagamento/store") }}
@endsection