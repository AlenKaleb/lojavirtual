@extends('panel.formapagamento._form')
@section('title')
    <h3>Edição de Forma de Pagamento</h3>
@endsection
@section('action')
    {{ url("formapagamento/update/{$formaPagamento->id}") }}
@endsection