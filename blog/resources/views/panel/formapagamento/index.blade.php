@extends('panel.layouts.model')
@section('content')
<!-- Datatables -->
<link href="{{ asset('panel/assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Formas de Pagamentos</h4>
                    <p class="category">Lista </p>
                </div>
                <div class="card-content table-responsive">
                    <a href="{{ url("formapagamento/create") }}">
                        <button class="btn btn-primary btn-fab btn-fab-mini btn-round">
                        <i class="material-icons">add</i>
                    </button> Nova Forma de Pagamento
                    </a>
                    <table id="datatable-buttons" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Forma de Pagamento</th>
                            <th>Editar</th>
                            <th>Remover</th>
                        </tr>
                        </thead>


                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!--   Core JS Files   -->
<script src="{{ asset('panel/assets/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>

<!-- Datatables -->
<script src="{{ asset('panel/assets/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('panel/assets/vendors/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="application/javascript">
    $('#datatable-buttons').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 10,
        "ordering": true,
        "ajax": {
            "url": "{{ url('formapagamento/paginate') }}",
            "type": "get",
            "data": function ( d ) {
                return d;
            }
        },
        "columns": [
            { "data": "nome" },
            { "data": "editar" },
            { "data": "remover" }
        ]
    } );
</script>
@endsection