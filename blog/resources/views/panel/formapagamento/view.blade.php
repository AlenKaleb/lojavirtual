@extends('panel.layouts.model')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Informações da forma de pagamento</h4>
                        <p class="category">forma de pagamento!</p>
                    </div>
                    <div class="card-content">
                        <div name="formFormaPagamento">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" disabled value="{{ $formaPagamento->nome }}">
                                    </div>
                                </div>
                            </div>
                            <a href="{{ url("formapagamento/edit/{$formaPagamento->id}") }}" type="button" class="btn btn-warning pull-right">Editar</a>
                            <a href="{{ url('formapagamento/index') }}" type="button" class="btn btn-primary pull-right">Voltar</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection