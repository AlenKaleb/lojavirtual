@extends('panel.layouts.model')
@section('content')
    <!-- Select2 -->
    <link href="{{ asset('panel/assets/vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">@yield('title')</h4>
                        <p class="category">Complete com seus dados!</p>
                    </div>
                    <div class="card-content">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form name="formFormaPagamento" action="@yield('action')" method="post">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nome</label>
                                        <input id="nome" name="nome" type="text" class="form-control" value="{{ (!empty($formaPagamento->nome)?$formaPagamento->nome:null) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                {{ csrf_field() }}
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Salvar</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection