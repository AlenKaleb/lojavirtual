@extends('site.layouts.model')
@section('content')
<?php
$subTotal = 0;
$total = 0;
$valorTaxas = 0;
?>
        <!-- Head Section -->
<section class="small-section bg-gray-lighter">
    <div class="relative container align-left">

        <div class="row">

            <div class="col-md-8">
                <h1 class="hs-line-11 font-alt mb-0">FINALIZAR COMPRA</h1>
            </div>

            <div class="col-md-4 mt-30">
                <div class="mod-breadcrumbs font-alt align-right">
                    <a href="#">Home</a>&nbsp;/&nbsp;<a href="#">Shop</a>&nbsp;/&nbsp;<span>Cart</span>
                </div>

            </div>
        </div>

    </div>
</section>
<!-- End Head Section -->


<!-- Section -->
<section class="page-section">
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @foreach($produtos as $produto)
                    @foreach($produto->produtoTaxa as $produtoTaxa)
                        <?php
                        $taxa = $produtoTaxa->taxa;
                        $valorTaxas += $taxa->valor;
                        ?>
                    @endforeach
                    <?php
                    $subTotal += $produto->valor_unitario;
                    ?>
                @endforeach
                <div class="row">
                    <div class="col-sm-8">
                        <form action="#" class="form">
                            <label for="formaPagamento">Forma de Pagamento:</label>
                            <select name="formaPagamento" class="mb-xs-10" style="width: 250px;" required >
                                <option>Selecione um item</option>
                                <option>Cartão de crédito</option>
                                <option>Boleto</option>
                            </select>
                        </form>
                    </div>
                    <div class="col-sm-4 text align-right">

                        <div>
                            <a href="" class="btn btn-mod btn-gray btn-round btn-small">Atualizar Carrinho</a>
                        </div>

                    </div>
                </div>

                <hr class="mb-60" />

                <div class="row">
                    <div class="col-sm-6 text align-right pt-10">

                        <?php
                        $total += ($subTotal + $valorTaxas);
                        ?>
                        <div>
                            Subtotal: <strong>R$ {{ number_format($subTotal,2,',','.') }}</strong>
                        </div>

                        <div class="mb-10">
                            Taxas: <strong>R$ {{ number_format($valorTaxas,2,',','.') }}</strong>
                        </div>

                        <div class="lead mt-0 mb-30">
                            Valor Total: <strong>R$ {{ number_format($total,2,',','.') }}</strong>
                        </div>

                        <div>
                            <a href="" class="btn btn-mod btn-round btn-large">Finalizar Compra</a>
                        </div>

                    </div>
                </div>



            </div>
        </div>

    </div>
</section>
<!-- End Section -->
@endsection