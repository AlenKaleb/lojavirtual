@extends('site.layouts.model')
@section('content')
    <?php
        $tagTecnologiaArr = explode(',',$produto->tag_tecnologia);

        $tagVantagemArr = explode(',',$produto->tag_vantagem);
    ?>
    <!-- Head Section -->
    <section class="small-section bg-gray-lighter">
        <div class="relative container align-left">

            <div class="row">

                <div class="col-md-8">
                    <h1 class="hs-line-11 font-alt mb-20 mb-xs-0">Detalhes do Produto</h1>
                    <div class="hs-line-4 font-alt black">
                        Informações
                    </div>
                </div>

                <div class="col-md-4 mt-30">
                    <div class="mod-breadcrumbs font-alt align-right">
                        <a href="#">Home</a>&nbsp;/&nbsp;<a href="#">Shop</a>&nbsp;/&nbsp;<span>Single</span>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End Head Section -->


    <!-- Section -->
    <section class="page-section">
        <div class="container relative">

            <!-- Product Content -->
            <div class="row mb-60 mb-xs-30">

                <!-- Product Images -->
                <div class="col-md-4 mb-md-30">

                    <div class="post-prev-img">
                        <a href="{{ url("painel/produtofotos/image/{$produto->fotos[0]->nome_gerado}") }}" class="lightbox-gallery-3 mfp-image"><img src="{{ url("painel/produtofotos/image/{$produto->fotos[0]->nome_gerado}") }}" alt="" /></a>
                        <div class="intro-label">
                            <span class="label label-danger bg-red">Sale</span>
                        </div>
                    </div>

                    <div class="row">
                        @foreach($produto->fotos as $foto)
                            <div class="col-xs-3 post-prev-img">
                                <a href="{{ url("painel/produtofotos/image/{$foto->nome_gerado}") }}" class="lightbox-gallery-3 mfp-image"><img src="{{ url("painel/produtofotos/image/{$foto->nome_gerado}") }}" alt="" /></a>
                            </div>
                        @endforeach
                    </div>

                </div>
                <!-- End Product Images -->

                <!-- Product Description -->
                <div class="col-sm-8 col-md-5 mb-xs-40">

                    <h3 class="mt-0">{{ $produto->nome }}</h3>

                    <hr class="mt-0 mb-30"/>

                    <div class="row">
                        <div class="col-xs-6 lead mt-0 mb-20">

                            <del class="section-text">$50.00</del>
                            <strong>R$ {{ number_format($produto->valor_unitario,2,',','.') }}</strong>

                        </div>
                        <div class="col-xs-6 align-right section-text">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            &nbsp;(3 reviews)
                        </div>
                    </div>

                    <hr class="mt-0 mb-30"/>

                    <div class="section-text mb-30">
                        {{ $produto->descricao }}
                    </div>

                    <hr class="mt-0 mb-30"/>

                    <div class="mb-30">
                        <form method="post" action="#" class="form">
                            <input type="number" class="input-lg round" min="1" max="1" value="1" />
                            <a href="#" class="btn btn-mod btn-large btn-round">Adicionar ao carrinho</a>
                        </form>
                    </div>

                    <hr class="mt-0 mb-30"/>

                    <div class="section-text small">
                        <div>ID: {{ $produto->id }}</div>
                        <div>Categoria: <a href=""> {{ $produto->nomeCategoria }}</a></div>
                        <div>Tags:
                            @foreach($tagTecnologiaArr as $tag)
                                <a href="#">{{ $tag }}</a>
                            @endforeach
                        </div>
                    </div>

                </div>
                <!-- End Product Description -->

                <!-- Features -->
                <div class="col-sm-4 col-md-3 mb-xs-40">

                    <!-- Features Item -->
                    <div class="alt-service-wrap">
                        <div class="alt-service-item">
                            <div class="alt-service-icon">
                                <i class="fa fa-comment"></i>
                            </div>
                            <h3 class="alt-services-title font-alt">TECNOLOGIAS</h3>
                            <div class="widget-body">
                                <div class="tags">
                                    @foreach($tagTecnologiaArr as $tag)
                                        <a href="#">{{ $tag }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="alt-service-wrap">
                        <div class="alt-service-item">
                            <div class="alt-service-icon">
                                <i class="fa fa-coffee"></i>
                            </div>
                            <h3 class="alt-services-title font-alt">COMPLEXIDADE</h3>
                            <strong>{{ $produto->complexidade }}</strong>
                        </div>
                    </div>
                    <!-- End Features Item -->

                    <!-- Features Item -->
                    <div class="alt-service-wrap">
                        <div class="alt-service-item">
                            <div class="alt-service-icon">
                                <i class="fa fa-gift"></i>
                            </div>
                            <h3 class="alt-services-title font-alt">VANTAGEM</h3>
                            <div class="widget-body">
                                <div class="tags">
                                    @foreach($tagVantagemArr as $tag)
                                        <a href="#">{{ $tag }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Features Item -->



                </div>
                <!-- End Features -->

            </div>
            <!-- End Product Content -->


            <!-- Nav tabs -->
            <ul class="nav nav-tabs tpl-tabs animate">
                <li class="active">
                    <a href="#one" data-toggle="tab">Descrição</a>
                </li>
                <li>
                    <a href="#two" data-toggle="tab">Tecnologias</a>
                </li>
                <li>
                    <a href="#three" data-toggle="tab">Vantagens</a>
                </li>
                <li>
                    <a href="#four" data-toggle="tab">Desvantagens</a>
                </li>
            </ul>
            <!-- End Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content tpl-tabs-cont">
                <div class="tab-pane fade in active" id="one">
                    <p>
                        {!! nl2br($produto->descricao) !!}
                    </p>
                </div>
                <div class="tab-pane fade" id="two">

                    <div class="widget-body">
                        <div class="tags">
                            @foreach($tagTecnologiaArr as $tag)
                                <a href="#">{{ $tag }}</a>
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="three">

                    <div class="widget-body">
                        <div class="tags">
                            @foreach($tagVantagemArr as $tag)
                                <a href="#">{{ $tag }}</a>
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="tab-pane fade" id="four">

                    <?php
                    $tagDesvantagem = explode(',',$produto->tag_desvantagem);
                    ?>
                    <div class="widget-body">
                        <div class="tags">
                            @foreach($tagDesvantagem as $tag)
                                <a href="#">{{ $tag }}</a>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
            <!-- End Tab panes -->

        </div>
    </section>
    <!-- End Section -->


    <!-- Divider -->
    <hr class="mt-0 mb-0 "/>
    <!-- End Divider -->


    <!-- Related Products -->
    <section class="page-section">
        <div class="container relative">

            <h2 class="section-title font-alt mb-70 mb-sm-40">
                Related Products
            </h2>

            <!-- Products Grid -->
            <div class="row multi-columns-row">

                <!-- Shop Item -->
                <div class="col-md-3 col-lg-3 mb-60 mb-xs-40">

                    <div class="post-prev-img">

                        <a href="shop-single.html"><img src="images/shop/shop-prev-1.jpg" alt="" /></a>

                        <div class="intro-label">
                            <span class="label label-danger bg-red">Sale</span>
                        </div>

                    </div>

                    <div class="post-prev-title font-alt align-center">
                        <a href="shop-single.html">G-Star Polo Applique Jersey</a>
                    </div>

                    <div class="post-prev-text align-center">
                        <del>$150.00</del>
                        &nbsp;
                        <strong>$94.75</strong>
                    </div>

                    <div class="post-prev-more align-center">
                        <a href="#" class="btn btn-mod btn-gray btn-round"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                    </div>

                </div>
                <!-- End Shop Item -->

                <!-- Shop Item -->
                <div class="col-md-3 col-lg-3 mb-60 mb-xs-40">

                    <div class="post-prev-img">
                        <a href="shop-single.html"><img src="images/shop/shop-prev-2.jpg" alt="" /></a>
                    </div>

                    <div class="post-prev-title font-alt align-center">
                        <a href="shop-single.html">Only & Sons Pique Polo Shirt</a>
                    </div>

                    <div class="post-prev-text align-center">
                        <strong>$28.99</strong>
                    </div>

                    <div class="post-prev-more align-center">
                        <a href="#" class="btn btn-mod btn-gray btn-round"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                    </div>

                </div>
                <!-- End Shop Item -->

                <!-- Shop Item -->
                <div class="col-md-3 col-lg-3 mb-60 mb-xs-40">

                    <div class="post-prev-img">
                        <a href="shop-single.html"><img src="images/shop/shop-prev-3.jpg" alt="" /></a>
                    </div>

                    <div class="post-prev-title font-alt align-center">
                        <a href="shop-single.html">Longline Long Sleeve</a>
                    </div>

                    <div class="post-prev-text align-center">
                        <strong>$39.99</strong>
                    </div>

                    <div class="post-prev-more align-center">
                        <a href="#" class="btn btn-mod btn-gray btn-round"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                    </div>

                </div>
                <!-- End Shop Item -->

                <!-- Shop Item -->
                <div class="col-md-3 col-lg-3 mb-60 mb-xs-40">

                    <div class="post-prev-img">
                        <a href="shop-single.html"><img src="images/shop/shop-prev-4.jpg" alt="" /></a>
                    </div>

                    <div class="post-prev-title font-alt align-center">
                        <a href="shop-single.html">Polo Shirt With Floral Sleeves</a>
                    </div>

                    <div class="post-prev-text align-center">
                        <strong>$85.99</strong>
                    </div>

                    <div class="post-prev-more align-center">
                        <a href="#" class="btn btn-mod btn-gray btn-round"><i class="fa fa-shopping-cart"></i> Add to cart</a>
                    </div>

                </div>
                <!-- End Shop Item -->
            </div>
            <!-- End Products Grid -->

        </div>
    </section>
    <!-- End Related Products -->
@endsection