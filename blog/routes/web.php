<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;


Route::get('/login', function () {
    return view('login.login');
});

Route::get('/index', function () {
    return view('site.index');
});

/**
 * Rotas do site
 * */
Route::get('produtos/index',['as' => 'produtos.index','uses' => 'ProdutoController@indexSite']);
Route::get('carrinho/index',['as' => 'carrinho','uses' => 'CarrinhoController@indexSite']);
//Route::get('index',['as' => 'produto.index','uses' => 'ProdutoController@index']);









/**
 * Rotas do painel
 * */
// Rota de Autenticação do usuário
Route::post('login',['as' => 'login','uses' => 'Auth\LoginController@authenticate']);

// Rota para verificação de email do usuário
Route::get('usuario/emailExist',['as' => 'usuario.emailExist','uses' => 'User\UserController@emailExist']);

// Rotas do carrinho de compra
Route::post('carrinho/store',['as' => '','uses' => 'CarrinhoController@store']);
//Route::post('carrinho/update/{id}',['as' => '','uses' => 'CarrinhoController@update']);
Route::get('carrinho/delete/{carrinho_id}/{produto_id}',['as' => '','uses' => 'CarrinhoController@destroy']);

Route::group(['middleware' => 'auth'],function() {

    Route::get('/painel', function () {
        return view('panel.index');
    });

    // Rota de Autenticação do usuário
    Route::get('logout',['as' => '','uses' => 'Auth\LoginController@logout']);

    /**
     * Rotas para usuários logados com permissão
     */

    // Rotas do Manter Funcionário
    Route::group(['prefix' => 'funcionario'],function(){
        Route::get('index',['as' => 'funcionario.index','uses' => 'FuncionarioController@index']);
        Route::get('pesquisarFuncionario',['as' => '','uses' => 'FuncionarioController@pesquisarFuncionario']);
        Route::get('view/{id}',['as' => 'funcionario.view','uses' => 'FuncionarioController@view']);
        Route::get('paginate',['as' => '','uses' => 'FuncionarioController@paginate']);
        Route::get('create',['as' => 'funcionario.create','uses' => 'FuncionarioController@create']);
        Route::get('edit/{id}',['as' => 'funcionario.edit','uses' => 'FuncionarioController@edit']);
        Route::post('store',['as' => '','uses' => 'FuncionarioController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'FuncionarioController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'FuncionarioController@destroy']);
    });

    // Rotas do Manter Cliente Fisico
    Route::group(['prefix' => 'fisico'],function(){
        Route::get('index',['as' => 'fisico.index','uses' => 'ClienteFisicoController@index']);
        Route::get('pesquisarClienteFisico',['as' => '','uses' => 'ClienteFisicoController@pesquisarClienteFisico']);
        Route::get('view/{id}',['as' => 'fisico.view','uses' => 'ClienteFisicoController@view']);
        Route::get('paginate',['as' => '','uses' => 'ClienteFisicoController@paginate']);
        Route::get('create',['as' => 'fisico.create','uses' => 'ClienteFisicoController@create']);
        Route::get('edit/{id}',['as' => 'fisico.edit','uses' => 'ClienteFisicoController@edit']);
        Route::post('store',['as' => '','uses' => 'ClienteFisicoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'ClienteFisicoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'ClienteFisicoController@destroy']);
    });

    // Rotas do Manter Cliente Juridico
    Route::group(['prefix' => 'juridico'],function(){
        Route::get('index',['as' => 'juridico.index','uses' => 'ClienteJuridicoController@index']);
        Route::get('pesquisarClienteJuridico',['as' => '','uses' => 'ClienteJuridicoController@pesquisarClienteJuridico']);
        Route::get('view/{id}',['as' => 'juridico.view','uses' => 'ClienteJuridicoController@view']);
        Route::get('paginate',['as' => '','uses' => 'ClienteJuridicoController@paginate']);
        Route::get('create',['as' => 'juridico.create','uses' => 'ClienteJuridicoController@create']);
        Route::get('edit/{id}',['as' => 'juridico.edit','uses' => 'ClienteJuridicoController@edit']);
        Route::post('store',['as' => '','uses' => 'ClienteJuridicoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'ClienteJuridicoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'ClienteJuridicoController@destroy']);
    });

// Rotas do Manter Categoria
Route::group(['prefix' => 'categoria'],function(){
    Route::get('index',['as' => 'categoria.index','uses' => 'CategoriaController@index']);
    Route::get('pesquisarCategoria',['as' => '','uses' => 'CategoriaController@pesquisarCategoria']);
    Route::get('view/{id}',['as' => 'categoria.view','uses' => 'CategoriaController@view']);
    Route::get('paginate',['as' => '','uses' => 'CategoriaController@paginate']);
    Route::get('create',['as' => 'categoria.create','uses' => 'CategoriaController@create']);
    Route::get('edit/{id}',['as' => 'categoria.edit','uses' => 'CategoriaController@edit']);
    Route::post('store',['as' => '','uses' => 'CategoriaController@store']);
    Route::post('update/{id}',['as' => '','uses' => 'CategoriaController@update']);
    Route::get('delete/{id}',['as' => '','uses' => 'CategoriaController@destroy']);
});

// Rotas do Manter Taxa
Route::group(['prefix' => 'taxa'],function(){
    Route::get('index',['as' => 'taxa.index','uses' => 'TaxaController@index']);
    Route::get('pesquisarTaxa',['as' => '','uses' => 'TaxaController@pesquisarTaxa']);
    Route::get('view/{id}',['as' => 'taxa.view','uses' => 'TaxaController@view']);
    Route::get('paginate',['as' => '','uses' => 'TaxaController@paginate']);
    Route::get('create',['as' => 'taxa.create','uses' => 'TaxaController@create']);
    Route::get('edit/{id}',['as' => 'taxa.edit','uses' => 'TaxaController@edit']);
    Route::post('store',['as' => '','uses' => 'TaxaController@store']);
    Route::post('update/{id}',['as' => '','uses' => 'TaxaController@update']);
    Route::get('delete/{id}',['as' => '','uses' => 'TaxaController@destroy']);
});

// Rotas do Manter Produto
Route::group(['prefix' => 'produto'],function(){
    Route::get('index',['as' => 'produto.index','uses' => 'ProdutoController@index']);
    Route::get('pesquisarProduto',['as' => '','uses' => 'ProdutoController@pesquisarProduto']);
    Route::get('view/{id}',['as' => 'produto.view','uses' => 'ProdutoController@view']);
    Route::get('detalhe/{id}',['as' => 'produtos.detalhe','uses' => 'ProdutoController@detalheSite']);
    Route::get('paginate',['as' => '','uses' => 'ProdutoController@paginate']);
    Route::get('create',['as' => 'produto.create','uses' => 'ProdutoController@create']);
    Route::get('edit/{id}',['as' => 'produto.edit','uses' => 'ProdutoController@edit']);
    Route::post('store',['as' => '','uses' => 'ProdutoController@store']);
    Route::post('update/{id}',['as' => '','uses' => 'ProdutoController@update']);
    Route::get('delete/{id}',['as' => '','uses' => 'ProdutoController@destroy']);
});

// Rotas do Manter Ticket
Route::group(['prefix' => 'ticket'],function(){
    Route::get('index',['as' => 'ticket.index','uses' => 'TicketController@index']);
    Route::get('pesquisarTicket',['as' => '','uses' => 'TicketController@pesquisarTicket']);
    Route::get('view/{id}',['as' => 'ticket.view','uses' => 'TicketController@view']);
    Route::get('paginate',['as' => '','uses' => 'TicketController@paginate']);
    Route::get('create',['as' => 'ticket.create','uses' => 'TicketController@create']);
    Route::get('edit/{id}',['as' => 'ticket.edit','uses' => 'TicketController@edit']);
    Route::post('store',['as' => '','uses' => 'TicketController@store']);
    Route::post('update/{id}',['as' => '','uses' => 'TicketController@update']);
    Route::get('delete/{id}',['as' => '','uses' => 'TicketController@destroy']);
});

    // Rotas do Manter Carrinho
    Route::group(['prefix' => 'carrinho'],function(){
          Route::get('checkout',['as' => 'carrinho.index','uses' => 'CarrinhoController@checkout']);
//        Route::get('index',['as' => 'produto.index','uses' => 'CarrinhoController@index']);
//        Route::get('view/{id}',['as' => 'carrinho.view','uses' => 'CarrinhoController@view']);
//        Route::get('detalhe/{id}',['as' => 'carrinho.detalhe','uses' => 'CarrinhoController@detalheSite']);
//        Route::get('paginate',['as' => '','uses' => 'CarrinhoController@paginate']);
//        Route::get('create',['as' => 'carrinho.create','uses' => 'CarrinhoController@create']);
//        Route::get('edit/{id}',['as' => 'carrinho.edit','uses' => 'CarrinhoController@edit']);
    });

    // Rotas do Manter Forma de Pagamento
    Route::group(['prefix' => 'formapagamento'],function(){
        Route::get('index',['as' => 'formapagamento.index','uses' => 'FormaPagamentoController@index']);
        Route::get('pesquisarFormaPagamento',['as' => '','uses' => 'FormaPagamentoController@pesquisarFormaPagamento']);
        Route::get('view/{id}',['as' => 'formapagamento.view','uses' => 'FormaPagamentoController@view']);
        Route::get('paginate',['as' => '','uses' => 'FormaPagamentoController@paginate']);
        Route::get('create',['as' => 'formapagamento.create','uses' => 'FormaPagamentoController@create']);
        Route::get('edit/{id}',['as' => 'formapagamento.edit','uses' => 'FormaPagamentoController@edit']);
        Route::post('store',['as' => '','uses' => 'FormaPagamentoController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'FormaPagamentoController@update']);
        Route::get('delete/{id}',['as' => '','uses' => 'FormaPagamentoController@destroy']);
    });


    // Rotas do Manter Venda
    Route::group(['prefix' => 'venda'],function(){
//        Route::get('index',['as' => 'venda.index','uses' => 'VendaController@index']);
        Route::get('view/{id}',['as' => 'venda.view','uses' => 'VendaController@view']);
        Route::get('detalhe/{id}',['as' => 'venda.detalhe','uses' => 'VendaController@detalheSite']);
        Route::get('paginate',['as' => '','uses' => 'VendaController@paginate']);
        Route::get('create',['as' => 'venda.create','uses' => 'VendaController@create']);
        Route::get('edit/{id}',['as' => 'venda.edit','uses' => 'VendaController@edit']);
        Route::post('store',['as' => '','uses' => 'VendaController@store']);
        Route::post('update/{id}',['as' => '','uses' => 'VendaController@update']);
        Route::get('delete/{carrinho_id}/{produto_id}',['as' => '','uses' => 'VendaController@destroy']);
    });

    // Rotas do Manter Pais
    Route::group(['prefix' => 'pais'],function(){
        Route::get('pesquisarPais',['as' => '','uses' => 'PaisController@pesquisarPais']);
    });

    // Rotas do Manter Estado
    Route::group(['prefix' => 'estado'],function(){
        Route::get('pesquisarEstado',['as' => '','uses' => 'EstadoController@pesquisarEstado']);
    });

    // Rotas do Manter Cidade
    Route::group(['prefix' => 'cidade'],function(){
        Route::get('pesquisarCidade',['as' => '','uses' => 'CidadeController@pesquisarCidade']);
    });

    // Download Route
    Route::get('download/{filename}', function($filename)
    {
        // Check if file exists in app/storage/file folder
        $file_path = storage_path() .'/app/arquivos/'. $filename;
        if (file_exists($file_path))
        {
            // Send Download
            return Response::download($file_path, $filename, [
                'Content-Length: '. filesize($file_path)
            ]);
        }
        else
        {
            // Error
            exit('Requested file does not exist on our server!');
        }
    })
        ->where('filename', '[A-Za-z0-9\-\_\.]+');

});

// Rota para leitura de imagens
Route::get('painel/{dir}/image/{filename}', function ($dir,$filename)
{
    $path = storage_path() . "/app/$dir/$filename";

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});