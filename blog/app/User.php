<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','usuario_id','usuario_tipo','permissao'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function funcionario(){
        return $this->hasOne('\App\Funcionario','user_id');
    }

    public function clienteFisico(){
        return $this->hasOne('\App\ClienteFisico','user_id');
    }

    public function clienteJuridico(){
        return $this->hasOne('\App\ClienteJuridico','user_id');
    }

}
