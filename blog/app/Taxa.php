<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxa extends Model
{
    protected $fillable = ['id','nome','valor','created_at','updated_at'];

    public $timestamps = true;

    protected $guarded = ['id','created_at','updated_at'];
}
