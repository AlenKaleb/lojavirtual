<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $fillable = ['id','estado_id','nome','created_at','updated_at'];

    public $timestamps = true;

    protected $guarded = ['id','created_at','updated_at'];
}
