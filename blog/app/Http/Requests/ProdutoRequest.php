<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:50',
            'qtd' => 'required|integer:true',
            'valor_unitario' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'categoria_id' => 'required|integer:true',
            'complexidade' => 'required|integer:true',
            'tag_tecnologia' => 'required|max:800',
            'tag_vantagem' => 'required|max:800',
            'tag_desvantagem' => 'required|max:800',
            'descricao' => 'required|max:800',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo :attribute não preenchido',
            'regex' => 'Campo :attribute esta com valor invalido',
            'integer' => 'Campo :attribute esta com valor invalido',
            'max' => 'Limite de :max caracters da :attribute foi excedido'
        ];
    }
}
