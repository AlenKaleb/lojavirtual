<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaxaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:50',
            'valor' => 'required|regex:/^\d*(\.\d{1,2})?$/',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo :attribute não preenchido',
            'max' => 'Limite de :max caracters da :attribute foi excedido',
            'regex' => 'Campo :attribute esta com valor inválido',
        ];
    }
}
