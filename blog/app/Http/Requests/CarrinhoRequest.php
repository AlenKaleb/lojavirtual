<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarrinhoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'produto_id' => 'required|integer:true',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo :attribute não preenchido',
            'date_format' => 'Campo :attribute com formato invalido',
            'integer' => 'Campo :attribute esta com valor invalido',
            'max' => 'Limite de :max caracters da :attribute foi excedido'
        ];
    }
}
