<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FuncionarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:50',
            'sexo' => 'required|max:1',
            'cpf' => 'required|max:20',
            'rg' => 'required|max:15',
            'email' => 'required|max:50',
            'password' => 'required|max:10'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Campo :attribute não preenchido',
            'regex' => 'Campo :attribute esta com valor invalido',
            'integer' => 'Campo :attribute esta com valor invalido',
            'date_format' => 'Campo :attribute com formato invalido',
            'max' => 'Limite de :max caracters da :attribute foi excedido'
        ];
    }
}
