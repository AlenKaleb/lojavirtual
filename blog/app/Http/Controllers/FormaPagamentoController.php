<?php

namespace App\Http\Controllers;

use App\FormaPagamento;
use App\Http\Requests\FormaPagamentoRequest;
use Illuminate\Http\Request;

class FormaPagamentoController extends Controller
{
    public function index(){
        return view('panel.formapagamento.index');
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'nome':'nome');
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $formaPagamentos = FormaPagamento::select('forma_pagamentos.id as id','nome')->where('nome','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($formaPagamentos);
        $recordsFiltered = null;
        foreach($formaPagamentos as $key => $value):
            $formaPagamentos[$key]['nome'] = "<a href=\"/formapagamento/view/{$value['id']}\">{$value['nome']}</a>";
            $formaPagamentos[$key] = array_add($formaPagamentos[$key],'editar', "<a href=\"/formapagamento/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $formaPagamentos[$key] = array_add($formaPagamentos[$key],'remover', "<a href=\"/formapagamento/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($formaPagamentos);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $formaPagamentos];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarFormaPagamento(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $formaPagamentos = FormaPagamento::select('id','nome')->where('nome','LIKE',"%{$nome}%")->get();
        return json_encode($formaPagamentos);
    }

    public function create(){
        return view('panel.formapagamento.create');
    }

    public function view($id){
        $formaPagamento = FormaPagamento::select('forma_pagamentos.id','nome')->where('forma_pagamentos.id',$id)->first();
        return view('panel.formapagamento.view',compact('formaPagamento'));
    }

    public function edit($id){
        $formaPagamento = FormaPagamento::select('forma_pagamentos.id','nome')->where('forma_pagamentos.id',$id)->first();
        return view('panel.formapagamento.update',compact('formaPagamento'));
    }

    public function store(FormaPagamentoRequest $request){
        $formaPagamento = FormaPagamento::create($request->all());
        return redirect("formapagamento/view/{$formaPagamento->id}");
    }

    public function update(FormaPagamentoRequest $request,$id){
//        $input = $request->all();
        FormaPagamento::find($id)->update($request->all());
        return redirect("formapagamento/view/{$id}");
    }

    public function destroy($id){
        FormaPagamento::find($id)->delete();
        return redirect('formapagamento/index');
    }
}
