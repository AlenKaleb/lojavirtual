<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaxaRequest;
use App\Taxa;
use Illuminate\Http\Request;

class TaxaController extends Controller
{
    public function index(){
        return view('panel.taxa.index');
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'nome':($_GET['order'][0]['column'] == 1?'valor':'nome'));
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $taxas = Taxa::select('taxas.id as id','nome','valor')->where('nome','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($taxas);
        $recordsFiltered = null;
        foreach($taxas as $key => $value):
            $taxas[$key]['nome'] = "<a href=\"/taxa/view/{$value['id']}\">{$value['nome']}</a>";
            $taxas[$key] = array_add($taxas[$key],'editar', "<a href=\"/taxa/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $taxas[$key] = array_add($taxas[$key],'remover', "<a href=\"/taxa/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($taxas);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $taxas];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarTaxa(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $taxas = Taxa::select('id','nome')->where('nome','LIKE',"%{$nome}%")->get();
        return json_encode($taxas);
    }

    public function create(){
        return view('panel.taxa.create');
    }

    public function view($id){
        $taxa = Taxa::select('taxas.id','nome','valor')->where('taxas.id',$id)->first();
        return view('panel.taxa.view',compact('taxa'));
    }

    public function edit($id){
        $taxa = Taxa::select('taxas.id','nome','valor')->where('taxas.id',$id)->first();
        return view('panel.taxa.update',compact('taxa'));
    }

    public function store(TaxaRequest $request){
        $taxa = Taxa::create($request->all());
        return redirect("taxa/view/{$taxa->id}");
    }

    public function update(TaxaRequest $request,$id){
//        $input = $request->all();
        Taxa::find($id)->update($request->all());
        return redirect("taxa/view/{$id}");
    }

    public function destroy($id){
        Taxa::find($id)->delete();
        return redirect('taxa/index');
    }
}
