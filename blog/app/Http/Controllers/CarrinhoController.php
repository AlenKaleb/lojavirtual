<?php

namespace App\Http\Controllers;

use App\Carrinho;
use App\CarrinhoProduto;
use App\Http\Requests\CarrinhoRequest;

use App\Produto;
use Illuminate\Support\Facades\Request;
use PDOException;

class CarrinhoController extends Controller
{
    public function indexSite(){
        $produtos = Produto::select('carrinho_produtos.produto_id as id','nome','valor_unitario')->leftJoin('carrinho_produtos','produtos.id','carrinho_produtos.produto_id')->leftJoin('carrinhos','carrinhos.id','carrinho_produtos.carrinho_id')->where('ip',Request::ip())->orderBy('carrinho_produtos.produto_id','desc')->get();
        return view('site.carrinho.index',compact('produtos'));
    }

    public function checkout(){
        $produtos = Produto::select('carrinho_produtos.produto_id as id','nome','valor_unitario')->leftJoin('carrinho_produtos','produtos.id','carrinho_produtos.produto_id')->leftJoin('carrinhos','carrinhos.id','carrinho_produtos.carrinho_id')->where('ip',Request::ip())->orderBy('carrinho_produtos.produto_id','desc')->get();
        return view('site.carrinho.checkout',compact('produtos'));
    }

    public function store(CarrinhoRequest $request){
        $ip = Request::ip();
        $findCarrinho = Carrinho::where('ip',$ip)->where('status',0)->orderBy('id','desc')->first();
        if(!empty($findCarrinho->id)):
            $request->merge(['carrinho_id' => $findCarrinho->id]);
        else:
            $request->merge(['data_hora_registro' => date('Y-m-d H:i:s')]);
            $request->merge(['ip' => $ip]);
            $request->merge(['status' => 0]);
            $carrinho = Carrinho::create($request->all());
            $request->merge(['carrinho_id' => $carrinho->id]);
        endif;
        $input = $request->all();
        $carrinhoProduto = CarrinhoProduto::select('carrinho_produtos.id')->where('carrinho_id',$input['carrinho_id'])->where('produto_id',$input['produto_id'])->first();
        if(empty($carrinhoProduto->id)):
            $carrinhoProdutoController = new CarrinhoProdutoController();
            $carrinhoProdutoController->store($request);
            $produto = Produto::select('carrinhos.id as carrinho_id','produtos.id as produto_id','nome','valor_unitario','nome_gerado')->leftJoin('carrinho_produtos','produtos.id','carrinho_produtos.produto_id')->leftJoin('carrinhos','carrinhos.id','carrinho_produtos.carrinho_id')->leftJoin('produto_fotos','produtos.id','produto_fotos.produto_id')->where('carrinho_produtos.carrinho_id',$input['carrinho_id'])->where('carrinho_produtos.produto_id',$input['produto_id'])->first();
            return $produto;
        else:
            return '';
        endif;

    }

//    public function update(CarrinhoRequest $request,$id){
////        $input = $request->all();
//        Carrinho::find($id)->update($request->all());
//        $request->merge(['carrinho_id' => $id]);
//        $carrinhoProdutoController = new CarrinhoProdutoController();
//        $carrinhoProdutoController->update($request);
//        return $id;
//    }

    public function destroy($carrinho_id,$produto_id){
        try{
            CarrinhoProduto::where('carrinho_id',$carrinho_id)->where('produto_id',$produto_id)->delete();
        }catch (PDOException $e){
            return $e->getMessage();
        }
    }
}
