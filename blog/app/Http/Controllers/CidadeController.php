<?php

namespace App\Http\Controllers;

use App\Cidade;
use Illuminate\Http\Request;

class CidadeController extends Controller
{
    public function pesquisarCidade(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $estado_id = FILTER_INPUT(INPUT_GET,'estado_id',FILTER_DEFAULT);
        $cidades = Cidade::select('id','nome')->where('nome','LIKE',"%{$nome}%")->where('estado_id',$estado_id)->get();
        return json_encode($cidades);
    }
}
