<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VendaController extends Controller
{
    public function indexSite(){
//        $ip = Request::ip();
//        $produtos = Produto::select('id','nome','valor_unitario')->get();
//        $categorias = Produto::select('categorias.nome',DB::raw('COUNT(produtos.id) as qtdProduto'))->leftJoin('categorias','categorias.id','produtos.categoria_id')->groupBy('categorias.id')->get();
//        $carrinhoProdutos = Produto::select('produtos.id','nome','valor_unitario','carrinho_id','produto_id','produtos.tag_tecnologia')->leftJoin('carrinho_produtos','produtos.id','carrinho_produtos.produto_id')->leftJoin('carrinhos','carrinhos.id','carrinho_produtos.carrinho_id')->where('ip',$ip)->where('carrinhos.status',0)->orderBy('carrinhos.id','desc')->get();
        return view('site.carrinho.checkout',compact('produtos','categorias','carrinhoProdutos'));
    }
}
