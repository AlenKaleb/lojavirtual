<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function store(Request $request){
        $input = $request->all();
        $user = new User();
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->save();
        return $user->id;
    }

    public function update(Request $request,$usuario_id){
        $input = $request->all();
        $user = User::find($usuario_id);
        $user->id = $usuario_id;
        $user->email = $input['email'];
        if(!empty($input['password'])):
            $user->password = bcrypt($input['password']);
        endif;
        $user->save();
        return $user->id;
    }

    public function destroy($usuario_id){
        \App\User::where('id',$usuario_id)->delete();
    }
}
