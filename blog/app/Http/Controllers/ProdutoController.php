<?php

namespace App\Http\Controllers;

use App\CarrinhoProduto;
use App\Http\Requests\ProdutoRequest;
use App\Produto;
use App\ProdutoFoto;
use App\Taxa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;


class ProdutoController extends Controller
{
    public function index(){
        return view('panel.produto.index');
    }

    public function indexSite(){
        $ip = Request::ip();
        $produtos = Produto::select('id','nome','valor_unitario')->get();
        $categorias = Produto::select('categorias.nome',DB::raw('COUNT(produtos.id) as qtdProduto'))->leftJoin('categorias','categorias.id','produtos.categoria_id')->groupBy('categorias.id')->get();
        $carrinhoProdutos = Produto::select('produtos.id','nome','valor_unitario','carrinho_id','produto_id','produtos.tag_tecnologia')->leftJoin('carrinho_produtos','produtos.id','carrinho_produtos.produto_id')->leftJoin('carrinhos','carrinhos.id','carrinho_produtos.carrinho_id')->where('ip',$ip)->where('carrinhos.status',0)->orderBy('carrinhos.id','desc')->get();
        return view('site.produtos.index',compact('produtos','categorias','carrinhoProdutos'));
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'nome':($_GET['order'][0]['column'] == 1?'categoria':($_GET['order'][0]['column'] == 2?'qtd':($_GET['order'][0]['column'] == 3?'valor_unitario':'nome'))));
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $produtos = Produto::select('produtos.id as id','produtos.nome','categorias.nome as categoria','qtd','valor_unitario')->leftJoin('categorias','categorias.id','produtos.categoria_id')->where('produtos.nome','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($produtos);
        $recordsFiltered = null;
        foreach($produtos as $key => $value):
            $produtos[$key]['nome'] = "<a href=\"/produto/view/{$value['id']}\">{$value['nome']}</a>";
            $produtos[$key] = array_add($produtos[$key],'editar', "<a href=\"/produto/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $produtos[$key] = array_add($produtos[$key],'remover', "<a href=\"/produto/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($produtos);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $produtos];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarProduto(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $produtos = Produto::select('id','nome')->where('nome','LIKE',"%{$nome}%")->get();
        return json_encode($produtos);
    }

    public function create(){
        return view('panel.produto.create');
    }

    public function view($id){
        $produto = Produto::select('produtos.id','produtos.nome','categoria_id','categorias.nome as nomeCategoria','qtd','complexidade','tag_tecnologia','tag_vantagem','tag_desvantagem','descricao','status','valor_unitario')->leftJoin('categorias','categorias.id','produtos.categoria_id')->where('produtos.id',$id)->first();
        $fotos = ProdutoFoto::select('nome_gerado')->where('produto_id',$produto->id)->get();
        $taxas = Taxa::select('taxas.id','nome')->leftJoin('produto_taxas','taxas.id','produto_taxas.taxa_id')->where('produto_id',$produto->id)->get();
        return view('panel.produto.view',compact('produto','taxas','fotos'));
    }

    public function detalheSite($id){
        $produto = Produto::select('produtos.id','produtos.nome','categoria_id','categorias.nome as nomeCategoria','qtd','complexidade','tag_tecnologia','tag_vantagem','tag_desvantagem','descricao','status','valor_unitario')->leftJoin('categorias','categorias.id','produtos.categoria_id')->where('produtos.id',$id)->first();
        $fotos = ProdutoFoto::select('nome_gerado')->get();
        $taxas = Taxa::select('taxas.id','nome')->leftJoin('produto_taxas','taxas.id','produto_taxas.taxa_id')->where('produto_id',$produto->id)->get();
        return view('site.produtos.detalhe',compact('produto','taxas','fotos'));
    }

    public function edit($id){
        $produto = Produto::select('produtos.id','produtos.nome','categoria_id','categorias.nome as nomeCategoria','qtd','complexidade','tag_tecnologia','tag_vantagem','tag_desvantagem','descricao','status','valor_unitario')->leftJoin('categorias','categorias.id','produtos.categoria_id')->where('produtos.id',$id)->first();
        $taxas = Taxa::select('taxas.id','nome')->leftJoin('produto_taxas','taxas.id','produto_taxas.taxa_id')->where('produto_id',$produto->id)->get();
        return view('panel.produto.update',compact('produto','taxas'));
    }

    public function store(ProdutoRequest $request){
        $input = $request->all();
        $request->merge(['status' => (!empty($input['status'])?1:0)]);
        $produto = Produto::create($request->all());
        $request->merge(['produto_id' => $produto->id]);
        $produtoFotoController = new ProdutoFotoController();
        $produtoFotoController->store($request,$produto->id);
        $produtoTaxaController = new ProdutoTaxaController();
        $produtoTaxaController->store($request);
        return redirect("produto/view/{$produto->id}");
    }

    public function update(ProdutoRequest $request,$id){
        $input = $request->all();
        $request->merge(['status' => (!empty($input['status'])?1:0)]);
        Produto::find($id)->update($request->all());
        $request->merge(['produto_id' => $id]);
        $produtoFotoController = new ProdutoFotoController();
        $produtoFotoController->update($request,$id);
        $produtoTaxaController = new ProdutoTaxaController();
        $produtoTaxaController->update($request,$id);
        return redirect("produto/view/{$id}");
    }

    public function destroy($id){
        $produtoFotoController = new ProdutoFotoController();
        $produtoFotoController->destroy($id);
        $produtoTaxaController = new ProdutoTaxaController();
        $produtoTaxaController->destroy($id);
        Produto::find($id)->delete();
        return redirect('produto/index');
    }
}
