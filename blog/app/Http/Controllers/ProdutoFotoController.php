<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProdutoRequest;
use App\ProdutoFoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProdutoFotoController extends Controller
{
    public function store(ProdutoRequest $request,$produto_id){
        $input = $request->all();
        $files = $request->file('imagem');
//                dd($files);
        if(!empty($files[0])):
            foreach($files as $file):
                if(!empty($file)):
                    $extension = $file->getClientOriginalExtension();
                    ProdutoFoto::create(['produto_id' => $produto_id,'nome_gerado' => $file->getFilename() . '.' . $extension,'tamanho' => $file->getClientSize(), 'tipo' => $extension]);
                    Storage::disk('local')->put('produtofotos/'.$file->getFilename().'.'.$extension,  File::get($file));
                endif;
            endforeach;
        endif;
    }

    public function update(ProdutoRequest $request,$produto_id){
        $input = $request->all();
        $files = $request->file('imagem');
        if(!empty($files[0])):
            $this->destroy($produto_id);
            $this->store($request,$produto_id);
        endif;
    }

    public function destroy($produto_id){
        $imagens = ProdutoFoto::where('produto_id',$produto_id)->get();
        foreach($imagens as $imagem):
            if(!empty($imagem->nome_gerado)):
                Storage::disk('local')->delete('produtofotos/'.$imagem->nome_gerado);
                ProdutoFoto::where('produto_id',$produto_id)->delete();
            endif;
        endforeach;
    }
}
