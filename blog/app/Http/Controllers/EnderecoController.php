<?php

namespace App\Http\Controllers;

use App\Endereco;
use Illuminate\Http\Request;

class EnderecoController extends Controller
{
    public function store(Request $request){
        Endereco::create($request->all());
    }

    public function update(Request $request,$id){
        $input = $request->all();
        Endereco::where('usuario_id',$input['usuario_id'])->where('usuario_tipo',$input['usuario_tipo'])->update(['cidade_id' => $input['cidade_id'],'cep' => $input['cep'],'logradouro' => $input['logradouro'],'numero' => $input['numero'],'bairro' => $input['bairro'],'complemento' => $input['complemento']]);
    }

    public function destroy($usuario_id,$usuario_tipo){
        Endereco::where('usuario_id',$usuario_id)->where('usuario_tipo',$usuario_tipo)->delete();
    }
}
