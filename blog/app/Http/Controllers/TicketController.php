<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketRequest;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function index(){
        return view('panel.ticket.index');
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'nome':($_GET['order'][0]['column'] == 1?'codigo':($_GET['order'][0]['column'] == 2?'desconto':'nome')));
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $tickets = Ticket::select('tickets.id as id','nome','codigo','desconto')->where('nome','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($tickets);
        $recordsFiltered = null;
        foreach($tickets as $key => $value):
            $tickets[$key]['nome'] = "<a href=\"/ticket/view/{$value['id']}\">{$value['nome']}</a>";
            $tickets[$key] = array_add($tickets[$key],'editar', "<a href=\"/ticket/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $tickets[$key] = array_add($tickets[$key],'remover', "<a href=\"/ticket/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($tickets);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $tickets];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarTicket(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $tickets = Ticket::select('id','nome')->where('nome','LIKE',"%{$nome}%")->get();
        return json_encode($tickets);
    }

    public function create(){
        return view('panel.ticket.create');
    }

    public function view($id){
        $ticket = Ticket::select('tickets.id','nome','codigo','desconto')->where('tickets.id',$id)->first();
        return view('panel.ticket.view',compact('ticket'));
    }

    public function edit($id){
        $ticket = Ticket::select('tickets.id','nome','codigo','desconto')->where('tickets.id',$id)->first();
        return view('panel.ticket.update',compact('ticket'));
    }

    public function store(TicketRequest $request){
        $ticket = Ticket::create($request->all());
        return redirect("ticket/view/{$ticket->id}");
    }

    public function update(TicketRequest $request,$id){
//        $input = $request->all();
        Ticket::find($id)->update($request->all());
        return redirect("ticket/view/{$id}");
    }

    public function destroy($id){
        Ticket::find($id)->delete();
        return redirect('ticket/index');
    }
}
