<?php

namespace App\Http\Controllers;

use App\ClienteJuridico;
use App\Email;
use App\Endereco;
use App\Http\Requests\ClienteJuridicoRequest;
use App\Pais;
use App\RestricaoAcesso;
use App\Telefone;
use App\User;
use Illuminate\Http\Request;

class ClienteJuridicoController extends Controller
{
    private static function before(ClienteJuridicoRequest $request){
        $dataRegistro = $request->input('data_registro');
        $dataRegistroArr = explode('/',$dataRegistro);
        $dataRegistroFormatado = "{$dataRegistroArr[2]}-{$dataRegistroArr[1]}-{$dataRegistroArr[0]}";

        $request->merge(['data_registro' => $dataRegistroFormatado]);
    }

    public function index(){
        return view('panel.juridico.index');
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'razao_social':($_GET['order'][0]['column'] == 1?'cnpj':($_GET['order'][0]['column'] == 2?'nome_fantasia':'razao_social')));
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $clienteJuridicos = ClienteJuridico::select('cliente_juridicos.id as id','razao_social','cnpj','nome_fantasia')->where('razao_social','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($clienteJuridicos);
        $recordsFiltered = null;
        foreach($clienteJuridicos as $key => $value):
            $clienteJuridicos[$key]['razao_social'] = "<a href=\"/juridico/view/{$value['id']}\">{$value['razao_social']}</a>";
            $clienteJuridicos[$key] = array_add($clienteJuridicos[$key],'editar', "<a href=\"/juridico/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $clienteJuridicos[$key] = array_add($clienteJuridicos[$key],'remover', "<a href=\"/juridico/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($clienteJuridicos);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $clienteJuridicos];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarClienteJuridico(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $clienteJuridicos = ClienteJuridico::select('id','razao_social')->where('razao_social','LIKE',"%{$nome}%")->get();
        return json_encode($clienteJuridicos);
    }

    public function create(){
        $paises = Pais::select('id','nome_pt')->get();
        return view('panel.juridico.create',compact('paises'));
    }

    public function view($id){
        $clienteJuridico = ClienteJuridico::select('cliente_juridicos.id','razao_social','cnpj','nome_fantasia','data_registro','inscricao_estadual','email')->leftJoin('users','cliente_juridicos.user_id','users.id')->where('cliente_juridicos.id',$id)->first();
        $clienteJuridico->data_registro = date_format(date_create($clienteJuridico->data_registro), 'd/m/Y');
        $telefones = Telefone::select('id','telefone')->where('usuario_id',$clienteJuridico->id)->where('usuario_tipo','J')->get();
        $emails = Email::select('id','email')->where('usuario_id',$clienteJuridico->id)->where('usuario_tipo','J')->get();
        $endereco = Endereco::select('cidade_id','logradouro','numero','bairro','cep','complemento','pais.id as pais_id','pais.nome_pt as nomePais','estados.id as estado_id','estados.nome as nomeEstado','cidades.id as cidade_id','cidades.nome as nomeCidade')->leftJoin('cidades','cidades.id','enderecos.cidade_id')->leftJoin('estados','estados.id','cidades.estado_id')->leftJoin('pais','pais.id','estados.pais_id')->where('usuario_id',$clienteJuridico->id)->where('usuario_tipo','J')->first();
        return view('panel.juridico.view',compact('clienteJuridico','telefones','emails','endereco'));
    }

    public function edit($id){
        $paises = Pais::select('id','nome_pt')->get();
        $clienteJuridico = ClienteJuridico::select('cliente_juridicos.id','razao_social','cnpj','nome_fantasia','data_registro','email','inscricao_estadual','user_id')->leftJoin('users','cliente_juridicos.user_id','users.id')->where('cliente_juridicos.id',$id)->first();
        $clienteJuridico->data_registro = date_format(date_create($clienteJuridico->data_registro), 'd/m/Y');
        $telefones = Telefone::select('id','telefone')->where('usuario_id',$clienteJuridico->id)->where('usuario_tipo','J')->get();
        $emails = Email::select('id','email')->where('usuario_id',$clienteJuridico->id)->where('usuario_tipo','J')->get();
        $endereco = Endereco::select('cidade_id','logradouro','numero','bairro','cep','complemento','pais.id as pais_id','pais.nome_pt as nomePais','estados.id as estado_id','estados.nome as nomeEstado','cidades.id as cidade_id','cidades.nome as nomeCidade')->leftJoin('cidades','cidades.id','enderecos.cidade_id')->leftJoin('estados','estados.id','cidades.estado_id')->leftJoin('pais','pais.id','estados.pais_id')->where('usuario_id',$clienteJuridico->id)->where('usuario_tipo','J')->first();
        return view('panel.juridico.update',compact('paises','clienteJuridico','telefones','emails','endereco'));
    }

    public function store(ClienteJuridicoRequest $request){
        $input = $request->all();
        self::before($request);
        $userController = new UserController();
        $usuario_id = $userController->store($request);
        $request->merge(['user_id' => $usuario_id]);
        $clienteJuridico = ClienteJuridico::create($request->all());
        User::find($usuario_id)->update(['usuario_id' => $clienteJuridico->id,'usuario_tipo' => 'J','permissao' => 0]);
        $request->merge(['usuario_id' => $clienteJuridico->id]);
        $request->merge(['usuario_tipo' => 'J']);
        $telefoneController = new TelefoneController();
        $telefoneController->store($request);
        $enderecoController = new EnderecoController();
        $enderecoController->store($request);
        return redirect("juridico/view/{$clienteJuridico->id}");
    }

    public function update(ClienteJuridicoRequest $request,$id){
        $input = $request->all();
        self::before($request);
        $request->merge(['usuario_id' => $id]);
        $request->merge(['usuario_tipo' => 'J']);
        $userController = new UserController();
        $userController->update($request,$input['user_id']);
        $telefoneController = new TelefoneController();
        $telefoneController->update($request,$id);
        $enderecoController = new EnderecoController();
        $enderecoController->update($request,$id);
        ClienteJuridico::find($id)->update($request->all());
        return redirect("juridico/view/{$id}");
    }

    public function destroy($id){
        $funcionario = ClienteJuridico::select('user_id')->where('id',$id)->first();
        $userController = new UserController();
        $userController->destroy($funcionario->user_id);
        $telefoneController = new TelefoneController();
        $telefoneController->destroy($id,'J');
        $enderecoController = new EnderecoController();
        $enderecoController->destroy($id,'J');
        ClienteJuridico::find($id)->delete();
        return redirect('juridico/index');
    }
}
