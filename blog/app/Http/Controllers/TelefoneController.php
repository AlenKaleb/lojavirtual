<?php

namespace App\Http\Controllers;

use App\Telefone;
use Illuminate\Http\Request;

class TelefoneController extends Controller
{
    public function store(Request $request){
        $input = $request->all();
        foreach($input['telefone'] as $key => $value):
            if(!empty($value)):
                $request->merge(['telefone' => $value]);
                Telefone::create($request->all());
            endif;
        endforeach;
    }

    public function update(Request $request,$usuario_id){
        $input = $request->all();
        $this->destroy($input['usuario_id'],$input['usuario_tipo']);
        $this->store($request);
    }

    public function destroy($usuario_id,$usuario_tipo){
        Telefone::where('usuario_id',$usuario_id)->where('usuario_tipo',$usuario_tipo)->delete();
    }
}
