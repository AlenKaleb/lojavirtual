<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate()
    {
        $email = FILTER_INPUT(INPUT_POST,'email',FILTER_DEFAULT);
        $password = FILTER_INPUT(INPUT_POST,'password',FILTER_DEFAULT);
        $usuario = User::where('email',$email)->first();
        if(!empty($usuario->id)):
            if (Auth::attempt(['email' => $email, 'password' => $password])):
                session_start();
                session(['permissao' => $usuario->permissao]);
                if($usuario->usuario_tipo == 'A'):
                    $funcionario = $usuario->funcionario;
                    session(['usuario' => $funcionario]);
                elseif($usuario->usuario_tipo == 'F'):
                    $clienteFisico = $usuario->clienteFisico;
                    session(['usuario' => $clienteFisico]);
                elseif($usuario->usuario_tipo == 'J'):
                    $clienteJuridico = $usuario->clienteJuridico;
                    session(['usuario' => $clienteJuridico]);
                endif;

                return redirect()->intended('/painel');
            else:
                return Redirect::to('login')->withErrors(array('password' => 'Ops! Parece que a senha está incorreta'))->withInput(Input::except('password'));
            endif;
        else:
            return Redirect::to('login')->withErrors(array('inativo' => 'Ops! Esta não existe ou está inativa!'))->withInput(Input::except('inativo'));
        endif;
    }

    public function logout(){
        session_start();
        session(['usuario' => null]);
        session(['permissao' => null]);

//        session()->flush();

        session_destroy();

        Auth::logout();
        return redirect('/login');
    }
}
