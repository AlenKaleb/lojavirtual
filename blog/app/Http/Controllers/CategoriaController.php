<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Http\Requests\CategoriaRequest;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    public function index(){
        return view('panel.categoria.index');
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'nome':'nome');
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $categorias = Categoria::select('categorias.id as id','nome')->where('nome','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($categorias);
        $recordsFiltered = null;
        foreach($categorias as $key => $value):
            $categorias[$key]['nome'] = "<a href=\"/categoria/view/{$value['id']}\">{$value['nome']}</a>";
            $categorias[$key] = array_add($categorias[$key],'editar', "<a href=\"/categoria/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $categorias[$key] = array_add($categorias[$key],'remover', "<a href=\"/categoria/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($categorias);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $categorias];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarCategoria(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $categorias = Categoria::select('id','nome')->where('nome','LIKE',"%{$nome}%")->get();
        return json_encode($categorias);
    }

    public function create(){
        return view('panel.categoria.create');
    }

    public function view($id){
        $categoria = Categoria::select('categorias.id','nome')->where('categorias.id',$id)->first();
        return view('panel.categoria.view',compact('categoria'));
    }

    public function edit($id){
        $categoria = Categoria::select('categorias.id','nome')->where('categorias.id',$id)->first();
        return view('panel.categoria.update',compact('categoria'));
    }

    public function store(CategoriaRequest $request){
        $categoria = Categoria::create($request->all());
        return redirect("categoria/view/{$categoria->id}");
    }

    public function update(CategoriaRequest $request,$id){
//        $input = $request->all();
        Categoria::find($id)->update($request->all());
        return redirect("categoria/view/{$id}");
    }

    public function destroy($id){
        Categoria::find($id)->delete();
        return redirect('categoria/index');
    }
}
