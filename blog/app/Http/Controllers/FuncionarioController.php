<?php

namespace App\Http\Controllers;

use App\Funcionario;
use App\Http\Requests\FuncionarioRequest;
use App\Pais;
use App\User;
use Illuminate\Http\Request;

class FuncionarioController extends Controller
{

    public function index(){
        return view('panel.funcionario.index');
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'nome':($_GET['order'][0]['column'] == 1?'cpf':($_GET['order'][0]['column'] == 2?'sexo':'nome')));
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $funcionarios = Funcionario::select('funcionarios.id as id','nome','cpf','sexo')->where('nome','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($funcionarios);
        $recordsFiltered = null;
        foreach($funcionarios as $key => $value):
            $funcionarios[$key]['nome'] = "<a href=\"/funcionario/view/{$value['id']}\">{$value['nome']}</a>";
            $funcionarios[$key] = array_add($funcionarios[$key],'editar', "<a href=\"/funcionario/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $funcionarios[$key] = array_add($funcionarios[$key],'remover', "<a href=\"/funcionario/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($funcionarios);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $funcionarios];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarFuncionario(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $funcionarios = Funcionario::select('id','nome')->where('nome','LIKE',"%{$nome}%")->get();
        return json_encode($funcionarios);
    }

    public function create(){
        $paises = Pais::select('id','nome_pt')->get();
        return view('panel.funcionario.create',compact('paises'));
    }

    public function view($id){
        $funcionario = Funcionario::select('funcionarios.id','nome','cpf','rg','sexo','email')->leftJoin('users','funcionarios.user_id','users.id')->where('funcionarios.id',$id)->first();
        return view('panel.funcionario.view',compact('funcionario'));
    }

    public function edit($id){
        $funcionario = Funcionario::select('funcionarios.id','nome','cpf','rg','sexo','email','user_id')->leftJoin('users','funcionarios.user_id','users.id')->where('funcionarios.id',$id)->first();
        return view('panel.funcionario.update',compact('funcionario'));
    }

    public function store(FuncionarioRequest $request){
        $userController = new UserController();
        $usuario_id = $userController->store($request);
        $request->merge(['user_id' => $usuario_id]);
        $funcionario = Funcionario::create($request->all());
        User::find($usuario_id)->update(['usuario_id' => $funcionario->id,'usuario_tipo' => 'A','permissao' => 1]);
        return redirect("funcionario/view/{$funcionario->id}");
    }

    public function update(FuncionarioRequest $request,$id){
        $input = $request->all();
        $userController = new UserController();
        $userController->update($request,$input['user_id']);
        Funcionario::find($id)->update($request->all());
        return redirect("funcionario/view/{$id}");
    }

    public function destroy($id){
        $funcionario = Funcionario::select('user_id')->where('id',$id)->first();
        $userController = new UserController();
        $userController->destroy($funcionario->user_id);
        Funcionario::find($id)->delete();
        return redirect('funcionario/index');
    }
}
