<?php

namespace App\Http\Controllers;

use App\Chat;
use App\ClienteFisico;
use App\Email;
use App\Endereco;
use App\Http\Requests\ClienteFisicoRequest;
use App\Pais;
use App\Projeto;
use App\RestricaoAcesso;
use App\Telefone;
use App\User;
use Illuminate\Http\Request;

class ClienteFisicoController extends Controller
{

    private static function before(ClienteFisicoRequest $request){
        $dataNascimento = $request->input('data_nascimento');
        $dataNascimentoArr = explode('/',$dataNascimento);
        $dataNascimentoFormatado = "{$dataNascimentoArr[2]}-{$dataNascimentoArr[1]}-{$dataNascimentoArr[0]}";

        $request->merge(['data_nascimento' => $dataNascimentoFormatado]);
    }

    public function index(){
        return view('panel.fisico.index');
    }

    public function paginate(){
//        return dd($_GET['search']['value']);
        $start = FILTER_INPUT(INPUT_GET,'start',FILTER_DEFAULT);
        $orderField = ($_GET['order'][0]['column'] == 0?'nome':($_GET['order'][0]['column'] == 1?'cpf':($_GET['order'][0]['column'] == 2?'sexo':'nome')));
        $orderValue = $_GET['order'][0]['dir'];
        $lenght = FILTER_INPUT(INPUT_GET,'length',FILTER_DEFAULT);
        $clienteFisicos = ClienteFisico::select('cliente_fisicos.id as id','nome','cpf','sexo')->where('nome','LIKE',"%{$_GET['search']['value']}%")->offset($start)->limit($lenght)->orderBy($orderField,$orderValue)->get()->toArray();
        $draw = FILTER_INPUT(INPUT_GET,'draw',FILTER_DEFAULT);
        $recordsTotal = count($clienteFisicos);
        $recordsFiltered = null;
        foreach($clienteFisicos as $key => $value):
            $clienteFisicos[$key]['nome'] = "<a href=\"/fisico/view/{$value['id']}\">{$value['nome']}</a>";
            $clienteFisicos[$key] = array_add($clienteFisicos[$key],'editar', "<a href=\"/fisico/edit/{$value['id']}\"><button class=\"btn btn-warning col col-sm-10\">Editar</button></a>");
            $clienteFisicos[$key] = array_add($clienteFisicos[$key],'remover', "<a href=\"/fisico/delete/{$value['id']}\"><button class=\"btn btn-danger col col-sm-10\">Remover</button></a>");
        endforeach;
//        dd($clienteFisicos);
        $json = ['draw' => $draw,'recordsTotal' => $recordsTotal,'recordsFiltered' => $recordsFiltered,'data' => $clienteFisicos];
//        dd($json);
        header('Content-Type: application/json');
        echo json_encode($json);
    }

    public function pesquisarClienteFisico(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $clienteFisicos = ClienteFisico::select('id','nome')->where('nome','LIKE',"%{$nome}%")->get();
        return json_encode($clienteFisicos);
    }

    public function create(){
        $paises = Pais::select('id','nome_pt')->get();
        return view('panel.fisico.create',compact('paises'));
    }

    public function view($id){
        $clienteFisico = ClienteFisico::select('cliente_fisicos.id','nome','cpf','rg','sexo','data_nascimento','email')->leftJoin('users','cliente_fisicos.user_id','users.id')->where('cliente_fisicos.id',$id)->first();
        $clienteFisico->data_nascimento = date_format(date_create($clienteFisico->data_nascimento), 'd/m/Y');
        $telefones = Telefone::select('id','telefone')->where('usuario_id',$clienteFisico->id)->where('usuario_tipo','F')->get();
        $emails = Email::select('id','email')->where('usuario_id',$clienteFisico->id)->where('usuario_tipo','F')->get();
        $endereco = Endereco::select('cidade_id','logradouro','numero','bairro','cep','complemento','pais.id as pais_id','pais.nome_pt as nomePais','estados.id as estado_id','estados.nome as nomeEstado','cidades.id as cidade_id','cidades.nome as nomeCidade')->leftJoin('cidades','cidades.id','enderecos.cidade_id')->leftJoin('estados','estados.id','cidades.estado_id')->leftJoin('pais','pais.id','estados.pais_id')->where('usuario_id',$clienteFisico->id)->where('usuario_tipo','F')->first();
        return view('panel.fisico.view',compact('clienteFisico','telefones','emails','endereco'));
    }

    public function edit($id){
            $paises = Pais::select('id','nome_pt')->get();
            $clienteFisico = ClienteFisico::select('cliente_fisicos.id','nome','cpf','rg','sexo','data_nascimento','email','user_id')->leftJoin('users','cliente_fisicos.user_id','users.id')->where('cliente_fisicos.id',$id)->first();
            $clienteFisico->data_nascimento = date_format(date_create($clienteFisico->data_nascimento), 'd/m/Y');
            $telefones = Telefone::select('id','telefone')->where('usuario_id',$clienteFisico->id)->where('usuario_tipo','F')->get();
            $emails = Email::select('id','email')->where('usuario_id',$clienteFisico->id)->where('usuario_tipo','F')->get();
            $endereco = Endereco::select('cidade_id','logradouro','numero','bairro','cep','complemento','pais.id as pais_id','pais.nome_pt as nomePais','estados.id as estado_id','estados.nome as nomeEstado','cidades.id as cidade_id','cidades.nome as nomeCidade')->leftJoin('cidades','cidades.id','enderecos.cidade_id')->leftJoin('estados','estados.id','cidades.estado_id')->leftJoin('pais','pais.id','estados.pais_id')->where('usuario_id',$clienteFisico->id)->where('usuario_tipo','F')->first();
            return view('panel.fisico.update',compact('paises','clienteFisico','telefones','emails','endereco'));
    }

    public function store(ClienteFisicoRequest $request){
        $input = $request->all();
        self::before($request);
        $userController = new UserController();
        $usuario_id = $userController->store($request);
        $request->merge(['user_id' => $usuario_id]);
        $clienteFisico = ClienteFisico::create($request->all());
        User::find($usuario_id)->update(['usuario_id' => $clienteFisico->id,'usuario_tipo' => 'F','permissao' => 0]);
        $request->merge(['usuario_id' => $clienteFisico->id]);
        $request->merge(['usuario_tipo' => 'F']);
        $telefoneController = new TelefoneController();
        $telefoneController->store($request);
        $enderecoController = new EnderecoController();
        $enderecoController->store($request);
        return redirect("fisico/view/{$clienteFisico->id}");
    }

    public function update(ClienteFisicoRequest $request,$id){
        $input = $request->all();
        self::before($request);
        $request->merge(['usuario_id' => $id]);
        $request->merge(['usuario_tipo' => 'F']);
        $userController = new UserController();
        $userController->update($request,$input['user_id']);
        $telefoneController = new TelefoneController();
        $telefoneController->update($request,$id);
        $enderecoController = new EnderecoController();
        $enderecoController->update($request,$id);
        ClienteFisico::find($id)->update($request->all());
        return redirect("fisico/view/{$id}");
    }

    public function destroy($id){
        $funcionario = ClienteFisico::select('user_id')->where('id',$id)->first();
        $userController = new UserController();
        $userController->destroy($funcionario->user_id);
        $telefoneController = new TelefoneController();
        $telefoneController->destroy($id,'F');
        $enderecoController = new EnderecoController();
        $enderecoController->destroy($id,'F');
        ClienteFisico::find($id)->delete();
        return redirect('fisico/index');
    }
}
