<?php

namespace App\Http\Controllers;

use App\CarrinhoProduto;
use App\Http\Requests\CarrinhoRequest;
use Illuminate\Http\Request;

class CarrinhoProdutoController extends Controller
{
    public function store(CarrinhoRequest $request){
        $carrinho = CarrinhoProduto::create($request->all());
        return $carrinho->id;
    }

    public function update(CarrinhoRequest $request,$produto_id){
//        $input = $request->all();
        CarrinhoProduto::find($produto_id)->update($request->all());
        return $produto_id;
    }

    public function destroy($produto_id){
        CarrinhoProduto::find($produto_id)->delete();
        return $produto_id;
    }
}
