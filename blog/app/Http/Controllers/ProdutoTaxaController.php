<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProdutoRequest;
use App\ProdutoTaxa;
use Illuminate\Http\Request;

class ProdutoTaxaController extends Controller
{
    public function store(ProdutoRequest $request){
        $input = $request->all();
        foreach($input['taxa_id'] as $key => $taxa):
            if(ctype_digit($taxa)):
                $request->merge(['taxa_id' => $taxa]);
                ProdutoTaxa::create($request->all());
            endif;
        endforeach;
    }

    public function update(ProdutoRequest $request,$id){
        $this->destroy($id);
        $this->store($request);
    }

    public function destroy($id){
        ProdutoTaxa::where('produto_id',$id)->delete();
    }
}
