<?php

namespace App\Http\Controllers;

use App\Email;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function store(Request $request){
        $input = $request->all();
        foreach($input['email'] as $key => $value):
            if(!empty($value)):
                $request->merge(['email' => $value]);
                Email::create($request->all());
            endif;
        endforeach;
    }

    public function update(Request $request,$usuario_id){
        $input = $request->all();
        $this->destroy($usuario_id,$input['usuario_tipo']);
        $this->store($request);
    }

    public function destroy($usuario_id,$usuario_tipo){
        Email::where('usuario_id',$usuario_id)->where('usuario_tipo',$usuario_tipo)->delete();
    }
}
