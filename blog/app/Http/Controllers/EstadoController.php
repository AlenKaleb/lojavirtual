<?php

namespace App\Http\Controllers;

use App\Estado;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    public function pesquisarEstado(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $pais_id = FILTER_INPUT(INPUT_GET,'pais_id',FILTER_DEFAULT);
        $estados = Estado::select('id','nome')->where('nome','LIKE',"%{$nome}%")->where('pais_id',$pais_id)->get();
        return json_encode($estados);
    }
}
