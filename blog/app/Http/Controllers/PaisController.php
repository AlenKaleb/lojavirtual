<?php

namespace App\Http\Controllers;

use App\Pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    public function pesquisarPais(){
        $nome = FILTER_INPUT(INPUT_GET,'term',FILTER_DEFAULT);
        $pais = Pais::select('id','nome_pt as nome')->where('nome_pt','LIKE',"%{$nome}%")->get();
        return json_encode($pais);
    }
}
