<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteJuridico extends Model
{
    protected $fillable = ['id','razao_social','nome_fantasia','cnpj','data_registro','inscricao_estadual','inscricao_municipal','user_id','created_at','updated_at'];

    public $timestamps = true;

    protected $guarded = ['id','created_at','updated_at'];
}
