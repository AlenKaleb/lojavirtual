<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestricaoAcesso extends Model
{
    public static function validarPermissao(){
        return session('permissao');
    }
}
