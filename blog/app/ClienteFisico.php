<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteFisico extends Model
{
    protected $fillable = ['id','nome','data_nascimento','sexo','cpf','rg','user_id','created_at','updated_at'];

    public $timestamps = true;

    protected $guarded = ['id','created_at','updated_at'];
}
