<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
    protected $fillable = ['id','telefone','usuario_id','usuario_tipo','created_at','updated_at'];

    public $timestamps = true;

    protected $guarded = ['id','created_at','updated_at'];
}
