<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrinhoProduto extends Model
{
    protected $fillable = ['id','produto_id','carrinho_id'];

    public $timestamps = false;

    protected $guarded = ['id'];

    public function carrinho(){
        return $this->belongsTo('\App\Carrinho','carrinho_id');
    }

    public function produto(){
        return $this->belongsTo('\App\Produto','produto_id');
    }
}
