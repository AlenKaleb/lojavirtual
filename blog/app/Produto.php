<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['id','nome','categoria_id','qtd','valor_unitario','complexidade','tag_tecnologia','tag_vantagem','tag_desvantagem','descricao','status','created_at','updated_at'];

    public $timestamps = true;

    protected $guarded = ['id','created_at','updated_at'];

    public function fotos(){
        return $this->hasMany('\App\ProdutoFoto','produto_id');
    }

    public function produtoTaxa(){
        return $this->hasMany('\App\ProdutoTaxa','produto_id');
    }

}
