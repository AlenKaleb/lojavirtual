<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoTaxa extends Model
{
    protected $fillable = ['id','produto_id','taxa_id'];

    public $timestamps = false;

    protected $guarded = ['id'];

    public function produto(){
        return $this->belongsTo('\App\Produto','produto_id');
    }

    public function taxa(){
        return $this->belongsTo('\App\Taxa','taxa_id');
    }

}
