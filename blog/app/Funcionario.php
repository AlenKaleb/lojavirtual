<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $fillable = ['id','nome','sexo','cpf','rg','user_id','created_at','updated_at'];
}
