<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdutoFoto extends Model
{
    protected $fillable = ['id','produto_id','nome_gerado','tipo','tamanho','created_at','updated_at'];

    public $timestamps = true;

    protected $guarded = ['id','created_at','updated_at'];
}
